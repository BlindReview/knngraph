#Summary

This readme file gives instructions on how to run the algorithms metioned in the paper **Efficiently Computing the k-NN Graph for High Dimensional Sparse Data**. 

We discuss the following steps:

1. Clone this repository to your machine
2. Compile the algorithms
3. Prepare the input files
4. Prepare the config file
5. Run the algorithm

#1. Clone this repository to your machine

Click on the actions tab, choose `clone` and follow the instructions. All subsequent actions will assume the cloned folder _~/knngraph/_ as root. We will refer to this folder as the working directory.

#2. Compile the algorithms

The source code for all algorithms can be found in the folder algorithms/. Navigate to this folder.

```
cd algorithms
```

Compile the source code.

```
make
```

We compiled the source with gcc version 4.6.3 (Ubuntu/Linaro 4.6.3-1ubuntu5) on Ubuntu 12.04.4 LTS

Return to the working directory.

```
cd ..
```

#3. Prepare the input files

For the datasets used in the paper, the inputfiles in the correct format are: 
```
dblp.csv
tweets_1M.csv
msd_users.csv
```

In a valid input file, every line represents one vector. Vector 0 is on line 1, vector 1 is on line 2, etc. On line l, there are the dimension-value pairs of the sparse vector (l-1). Dimensions and values are alternated, separated by commas, without any further markup. All dimensions are respresented by integers and if there are m dimensions, they are identified by the numbers 0 to (m-1).

#4. Prepare the config file

All algorithms take their input from the file `config.txt`. All lines in this file end with a colon. The first line contains the path to the inputfile. The second line contains the path of the file to which the runtimes are written. The third line contains the path of the file to which the actual nearest neighbors are written. Every line of this file starts with a target object, and is followed by its nearest neighbors. The fourth line of the config file contains the parameter k, i.e. the size of the neighborhood that needs to be computed. The fifth line contains the name of the algorithm that is to be used. Consistent with the naming in the paper, the options are: `naive_baseline`, `basic_inverted_index`, `dynamicIndex`, `prunedIndex`, `sc`, and `maxScore`.
#4. Run the algorithm

From the directory `~/knngraph/`, the algorithm can be executed with the command

```
./algorithms/compute_all_top_k config.txt
```
Keep in mind that the runtime of the algorithms is heavily influenced by multiple processes using the same cpu-cache. Therefore, it is recommended to give the algorithm a dedicated node on which no other jobs are running.


