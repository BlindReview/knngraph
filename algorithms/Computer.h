 /*----------------------------------------------------------------------
  File     : computer.h
  Contents : definition of object that computes top k most similar objects for each object
  Update   : February 2015
----------------------------------------------------------------------*/
#ifndef COMPUTER_H
#define COMPUTER_H

#include <vector> //vector
#include <string> //string
#include <iostream> //cout
#include <fstream> //ifstream
#include <tuple> //tuple
#include <unordered_map>
#include <iterator>
#include <list>
#include <queue>
#include <time.h>	
#include <unordered_map>
#include "skiplist_static.h"

typedef std::pair<unsigned int,unsigned int> frontierElementType;
typedef std::vector<std::pair<unsigned int,double>>::iterator postingIteratorType;

class Compare_pairs_second_bib{
public:
	bool operator()(std::pair<unsigned int,double> pair1, std::pair<unsigned int,double> pair2){
		return (pair1.second > pair2.second);
	}

};

class Compare_postings_first_bgf{
public:
	bool operator()(frontierElementType p1, frontierElementType p2){
		if(p1.first>p2.first) return true;
		else if(p1.first==p2.first){
			if(p1.second>p2.second) return true;
			else return false;
		} else return false;
	}

};

class DescendingMaxScore{
public:
	DescendingMaxScore(std::vector<skiplist_static<double>*>* pObjectsByFeature){
		p_objectsByFeature = pObjectsByFeature;
	};
	~DescendingMaxScore(){};

	bool operator()(std::pair<unsigned int,double> p1, std::pair<unsigned int,double> p2){
		return ((*p_objectsByFeature)[p1.first]->maxScore > (*p_objectsByFeature)[p2.first]->maxScore);
	}

protected:
	std::vector<skiplist_static<double>*>* p_objectsByFeature;

};

class DescendingMaxScore_slim{
public:
	DescendingMaxScore_slim(std::unordered_map<unsigned int,double>& maxWByFeatureId){
		i_maxWByFeatureId = maxWByFeatureId;
	};
	~DescendingMaxScore_slim(){};

	bool operator()(std::pair<unsigned int,double> p1, std::pair<unsigned int,double> p2){
		return (p1.second*i_maxWByFeatureId[p1.first] > p2.second*i_maxWByFeatureId[p2.first]);
	}

protected:
	std::unordered_map<unsigned int,double> i_maxWByFeatureId;

};


typedef std::priority_queue<std::pair<unsigned int,double>, std::vector<std::pair<unsigned int,double>>, Compare_pairs_second_bib> topkType;
typedef std::priority_queue<double, std::vector<double>> thresholdType;
typedef std::priority_queue<frontierElementType, std::vector<frontierElementType>, Compare_postings_first_bgf> frontierType;

class Computer {
public:
	Computer():k(1),maxSim(1.0),minSim(0.0),m(1),addLeaderRoof(1){};
	~Computer(){};
	bool readObject(std::ifstream &inputFile, std::vector<std::string> &splitted);
	void baseline(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void setk(unsigned int k_in);
	void setm(unsigned int m_in);
	void setThreshold(double t_in);
	void allNN(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void sc(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void non_indexed_baseline(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void maxScore(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	std::tuple<unsigned int,unsigned int,std::vector<double>,std::vector<unsigned int>> getGlobalInfoPlus(std::string inputFileName);
	void fillSkiplists(std::string inputFileName, std::vector<skiplist_static<double>*>& objectsByFeature, std::vector<double>& norms);
	void dynamicIndex_heap(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void frozenIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<bool>& finishedByNewObjectId, std::vector<topkType>& topkByNewObjectId, unsigned int noObjects, unsigned int noFeatures);
	void dynamicIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<unsigned int>& unfinishedObjects, std::vector<topkType>& topkByNewObjectId, unsigned int noObjects, unsigned int noFeatures, std::vector<double>& maxWByNewFeatureId);
	void skipToObject(postingIteratorType& pit, postingIteratorType endOfPosting, unsigned int objectId, bool& isMatch, bool& isFinished);
	void transferIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& index, std::vector<std::vector<std::pair<unsigned int,double>>>& objects);
	void emptyIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& index);
	void idFinishedObjects(std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, double threshold, std::vector<bool>& unfinishedByNewObjectId);
	void idUnfinishedObjects(std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<unsigned int>& unfinishedObjects, std::vector<bool>& finishedByNewObjectId, double threshold);
	void restoreObjects_allnn(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature);
	void restoreObjects(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void reCodeFeatures(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& norms, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& maxValByNewObjectId, std::vector<double>& maxValByNewFeatureId);
	void readData(std::string inputFileName, unsigned int N, std::unordered_map<unsigned int, std::vector<std::pair<unsigned int,double>>>& objectsByFeature, unsigned int& noObjects, unsigned int& noSegments);
	void readObjects(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& objects);
	void readObjects(std::string inputFileName, std::vector<double>& normByObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<size_t>& sizeByNewObjectId, std::vector<double>& bByObject, double t, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId);
	void readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, std::vector<size_t>& sizeByNewObjectId, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId);
	void readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<size_t>& sizeByNewObjectId);
	void readData(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, unsigned int& noFeatures, std::vector<double>& norms, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& maxValByNewFeatureId, std::vector<double>& maxValByNewObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId);
	void updateOffset_allnn(unsigned int& offset, std::vector<std::tuple<unsigned int,double,double>>::iterator currentStart, std::vector<std::tuple<unsigned int,double,double>>::iterator end, std::vector<size_t>& sizeByNewObjectId, double minSize);
	void updateOffset(unsigned int& offset, std::vector<std::pair<unsigned int,double>>::iterator currentStart, std::vector<std::pair<unsigned int,double>>::iterator end, std::vector<size_t>& sizeByNewObjectId, double minSize);
	void updateOffset(std::vector<std::vector<std::tuple<unsigned int,double,double>>>& followersByFeature, unsigned int& offset, double minSim, std::vector<std::vector<std::pair<unsigned int, double>>>& objects, unsigned int size, unsigned int featureId, std::vector<unsigned int>& newByOldObjectId);
	void fillObjectFloorPairs(std::list<std::pair<unsigned int,double>>& objectFloorPairs, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<bool>& followerByOldId);
	void extractGlobalStats_mw(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId);
	void extractGlobalStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId);
	void extractGlobalStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::unordered_map<unsigned int,double>& sumByOldObjectId);
	void extractStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::list<std::pair<unsigned int,double>>& objectFloorPairs, std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<double>& norms);
	void extractStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::list<std::pair<unsigned int,double>>& objectFloorPairs, std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs);
	void mapObjects_bgf(std::vector<std::pair<unsigned int, double>>& objectValPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId,std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::vector<double>& maxWByNewObjectId, std::unordered_map<unsigned int,double>& sumByOldObjectId, std::vector<double>& sumByNewObjectId);
	void mapObjects_bgf(std::vector<std::pair<unsigned int, double>>& objectValPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId,std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::vector<double>& maxWByNewObjectId);
	void mapObjects(std::vector<std::pair<unsigned int, double>>& objectFloorPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId);
	void mapObjects(std::list<std::pair<unsigned int, double>>& objectFloorPairs, std::vector<double>& floorByNewObjectId, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId);
	void mapFeatures_bgf(std::vector<std::pair<unsigned int, double>>& featureValPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId,std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::vector<double>& maxWByNewFeatureId);
	void mapFeatures(std::vector<std::pair<unsigned int, double>>& featureValPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId);
	void mapFeatures(std::vector<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId);
	void mapFeatures(std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId);
	void mapFeatures(std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& countByNewFeatureId, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId);
	void loadObjects(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewFeatureId, std::vector<double>& norms);
	void loadObjects(std::string inputFileName, std::vector<std::list<std::pair<unsigned int,double>>>& objects, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewFeatureId, std::vector<double>& norms);
	double half_dense_dot(std::vector<std::pair<unsigned int,double>>& v1, std::unordered_map<unsigned int,double>& v2);
	double half_dense_dot(std::vector<std::pair<unsigned int,double>>& v1, std::vector<double>& v2);
	double dot(std::vector<std::tuple<unsigned int,double,double>>& v1, std::vector<std::tuple<unsigned int,double,double>>& v2);
	double dot(std::vector<std::pair<unsigned int,double>>& v1, std::vector<std::pair<unsigned int,double>>& v2);
	double dot(std::list<std::pair<unsigned int,double>>& v1, std::list<std::pair<unsigned int,double>>& v2);
	void writeTopks(std::string controlFileName, std::vector<topkType>& topkByObject, std::vector<unsigned int>& oldByNewObjectId);
	void writeTopks_elimDups_withSim(std::string controlFileName, std::vector<topkType>& topkByNewObjectId, std::vector<unsigned int>& oldByNewObjectId);
	void writeTopks_elimDups(std::string controlFileName, std::vector<topkType>& topkByNewObjectId, std::vector<unsigned int>& oldByNewObjectId);
	void writeTopks_elimDups(std::string controlFileName, std::vector<topkType>& topkByObject);
	void writeTopks_withSim(std::string controlFileName, std::vector<topkType>& topkByObject);
	void writeTopks(std::string controlFileName, std::vector<topkType>& topkByObject);
	void writeTopks(std::string controlFileName, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject);
	void processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, double norm, std::vector<unsigned int>& newByOldFeatureId);
	void processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, double norm);
	void processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures);
	void processObject(unsigned int objectIndex, std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<double>& norms, DescendingMaxScore comparator);
	void completeSimilarities_allnn(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<double>& pscore, std::vector<double>& prefixSqNorm,unsigned int noFeatures, std::vector<double>& sumByNewObjectId);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<unsigned int>& offsetByObject, std::vector<unsigned int>& postfixOffsetByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<unsigned int>& offsetByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<unsigned int>& offsetByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<unsigned int>& offsetByObject, std::vector<unsigned int>& postfixOffsetByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& postfixes, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::pair<unsigned int,double>>& targetFeatures, unsigned int noFeatures);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, unsigned int newIndex, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewObjectId, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, unsigned int noFeatures);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, unsigned int newIndex, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewObjectId, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewObjectId, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<unsigned int>& newByOldObjectId);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& norms);
	void completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex);
	void computeSimilarities_dynamicIndex(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void computeSimilarities_allnn(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::tuple<unsigned int, double,double>>& targetFeatures, std::vector<std::pair<unsigned int, double>>& postfixFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::tuple<unsigned int, double, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<unsigned int>& offsetByFeature, std::vector<unsigned int>& newByOldObjectId);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double& maxIndexSim);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& norms, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::unordered_map<unsigned int, std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::list<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::list<std::pair<unsigned int,double>>>& objectsByFeature);
	void computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& norms, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, topkType& outputTopk, std::pair<unsigned int,double>& inTailSize, unsigned int targetIndex);
	void initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<topkType>& topkByObject, std::pair<unsigned int,double>& inTailSize, unsigned int targetIndex);
	void initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject, std::pair<double,unsigned int>& minSim);
	void updateLinkedTopks(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<topkType>& topkByObject, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject);
	void updateLinkedTopks(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject, std::vector<std::pair<double,unsigned int>>& minSimByObject);
	void updateTopk(topkType& topk, std::vector<std::pair<unsigned int,double>>& simPairs, std::pair<unsigned int,double>& inTailSize);
	void updateTopk(topkType& topk, unsigned int candidate, double similarity , std::pair<unsigned int,double>& inTailSize, bool& changedMinScore);
	void updateTopk(topkType& topk, unsigned int candidate, double similarity , std::pair<unsigned int,double>& inTailSize, unsigned int topkId);
	void updateTopk(std::list<std::pair<unsigned int,double>>& topk, unsigned int candidate, double similarity , double& min, unsigned int& nMin);
	void resetTopk(topkType& topk, std::pair<unsigned int,double>& inTailSize);
	void resetTopk(std::list<std::pair<unsigned int,double>>& topk, double& min, unsigned int& nMin);
	void handleNewMin(topkType& topk, std::pair<unsigned int,double>& inTailSize);
	void addToIndex_allnn(unsigned int targetIndex, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& pscore, std::vector<double>& prefixSqNorm, double& targetSum);
	void addToIndex_sorted(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::pair<unsigned int,double>>& object, unsigned int& objectOffset, unsigned int& increment, double& b, std::vector<double>& maxWByNewFeatureId, double& maxObjectW);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::tuple<unsigned int,double,double>>& object, unsigned int& objectOffset);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::tuple<unsigned int,double,double>>& object, unsigned int& objectOffset, unsigned int& increment);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects);
	void addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects);
	void addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects);
	void addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal);
	void addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal);
	void addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId);
	void addToIndex(unsigned int targetIndex, std::list<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::list<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId);
	void addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void baseline_p(std::string inputFileName, std::string timingFileName, std::string controlFileName);
	void computeSimilarities(std::vector<double>& similarities, std::vector<double>& norms, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, unsigned int targetIndex);
	void extractTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::list<unsigned int>& topk);
	void fillTopk(std::vector<double> norms, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, std::vector<double>& similarities, std::list<unsigned int>& topk, std::vector<bool>& inTopk, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void initMinTopk(double& min, unsigned int& nMin, std::list<unsigned int>& topk, std::vector<double>& similarities);
	void resetMinTopk(double& min, unsigned int& nMin, std::list<unsigned int>& topk, std::vector<double>& similarities, double startMin);
	void updateTopk(std::vector<double> norms, unsigned int targetIndex, std::list<unsigned int>& topk, double& min, unsigned int& nMin, std::vector<double>& similarities, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double startMin);
	void processOverlap(std::vector<double> norms, unsigned int targetIndex, std::list<unsigned int>& topk, double& min, unsigned int& nMin, std::vector<double>& similarities, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, double startMin);
	void writeTopk(std::ofstream& controlFile, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& topk);
	void writeTopk(unsigned int targetIndex, std::list<unsigned int>& topk, std::ofstream& controlFile);
	void writeTopk(std::ofstream& controlFile, topkType& topk, unsigned int targetIndex);
	void writeTiming(std::string timingFileName, clock_t recordedTime, std::string timeDescription);
	void writeDouble(std::string fileName, double doubleNumber, std::string doubleDescription);
	std::tuple<unsigned int,unsigned int,std::vector<double>,std::list<std::pair<unsigned int,unsigned int>>> getGlobalStats(std::string inputFileName);
	std::tuple<unsigned int,unsigned int,std::vector<double>> getGlobalInfo(std::string inputFileName);
	std::tuple<unsigned int,unsigned int,std::vector<double>> getGlobalInfo(std::string inputFileName, std::vector<double>& maxValByFeatureId, std::vector<std::pair<unsigned int, double>>& featureValPairs);
	void fillDoubleIndex(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& norms);
	void fillDoubleIndex(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature);
	void estimate_threshold(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int noObjects, unsigned int noFeatures, double& estimated_threshold);
	void extractAtk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& kmostSimilars);

private:
	unsigned int k;
	double maxSim;
	double minSim;
	unsigned int m; //multiplication of k for top objects per feature
	double addLeaderRoof;
	double threshold;
	std::vector<double> t_vec;

};

#endif