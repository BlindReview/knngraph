//////////////////////////////////////////////////////////////////////////////
 
#ifndef _SKIPLIST_STATIC_H_
#define _SKIPLIST_STATIC_H_
 
#include <iostream>
#include <sstream>
#include <math.h>
#include <stdlib.h>
#include <algorithm> 
 
///////////////////////////////////////////////////////////////////////////////
 
template<class V>
class skiplist_node
{
public:
    skiplist_node(unsigned int LEVEL):level(LEVEL)
    {
        for (unsigned int i=0; i<LEVEL; i++ ) {
            forwards.push_back(NULL);
        }
        forwards.shrink_to_fit();
    }
 
    skiplist_node(unsigned int internalKey,unsigned int LEVEL):level(LEVEL),internalKey(internalKey)
    {
        for (unsigned int i=0; i<LEVEL; i++ ) {
            forwards.push_back(NULL);
        }
        forwards.shrink_to_fit();
    }
 
    skiplist_node(unsigned int internalKey,V val,unsigned int LEVEL,unsigned int list):listId(list),level(LEVEL),internalKey(internalKey),value(val)
    {
        for (unsigned int i=0; i<LEVEL; i++ ) {
            forwards.push_back(NULL);
        }
        forwards.shrink_to_fit();
    }

    unsigned int getKey(){
        return internalKey-1;
    }
 
    virtual ~skiplist_node()
    {
    }
 
    unsigned int listId;
    unsigned int level;
    unsigned int internalKey;
    V value;
    std::vector<skiplist_node<V>*> forwards;
};
 
///////////////////////////////////////////////////////////////////////////////
 
template<class V>
class skiplist_static
{
public:
    typedef V ValueType;
    typedef skiplist_node<V> NodeType;
 
    skiplist_static(unsigned int minKey,unsigned int maxKey, unsigned int MAXLEVEL, unsigned int id):id(id),length(0),maxScore(0),m_minKey(minKey),m_maxKey(maxKey+2),m_pHeader(NULL),m_pTail(NULL)
    {
        m_pHeader = new NodeType(m_minKey,MAXLEVEL); //pointer to node object;
        m_pTail = new NodeType(m_maxKey,MAXLEVEL); //pointer to node opbject; should be (theoretical) maxKey+2, if minkey=0 and every real key(starting with 0) is internally incremented with 1 to accomodate for the header being 0, right?
        for (unsigned int i=0; i<MAXLEVEL; i++ ) {
            m_pHeader->forwards[i] = m_pTail;
            toTail.push_back(m_pHeader);
        }
    }
 
    virtual ~skiplist_static()
    {
        NodeType* currNode = m_pHeader->forwards[0];
        while ( currNode != m_pTail ) {
            NodeType* tempNode = currNode;
            currNode = currNode->forwards[0];
            delete tempNode;
        }
        delete m_pHeader;
        delete m_pTail;
    }

    void push_back(unsigned int externalKey, V newValue){
        unsigned int internalKey = externalKey+1;
        ++length;
        maxScore = std::max(maxScore,newValue);
        int level = deterministicLevel();
        NodeType* p_novelNode = new NodeType(internalKey,newValue,level,id);
        for( int l=0; l<level; ++l){
            p_novelNode->forwards[l] = m_pTail;
            toTail[l]->forwards[l] = p_novelNode;
            toTail[l] = p_novelNode;
        }
    }//end of push_back

    bool advance(NodeType*& iterator){
        iterator = iterator->forwards[0];
        if(iterator->internalKey==m_maxKey) return false;
        else return true;
    }

    NodeType* getStart(){
        NodeType* returnPointer = m_pHeader;
        advance(returnPointer);
        return returnPointer;
    }
 
    bool skipTo(NodeType*& iterator, unsigned int externalKey){
        //internalKey needs to be smaller than maxKey !!
        unsigned int internalKey = externalKey+1;
        if(iterator->internalKey >= internalKey) return true;
        else {
            unsigned int upLevel = iterator->level-1;
            while ( iterator->forwards[upLevel]->internalKey < internalKey) {
                iterator = iterator->forwards[upLevel];
                upLevel = iterator->level-1;
            }
            for(int downLevel=upLevel-1; downLevel >0; downLevel--) {
                while ( iterator->forwards[downLevel]->internalKey < internalKey) {
                    iterator = iterator->forwards[downLevel];
                }
            }
            iterator = iterator->forwards[0];
            if(iterator->internalKey==m_maxKey) return false;
            else return true;
        }
    }//en of skipTo
 
    unsigned int id;
    int length;
    double maxScore;
 
protected:

    int deterministicLevel(){
        int level = 1;
        int max_add_level = floor(log2(length));
        int al = max_add_level;
        bool searching = true;
        while( (al>0) && searching ){
            div_t divres = div(length,al);
            if(divres.rem == 0) level += al, searching = false;
            --al;
        }
        return level;

    }

    unsigned int m_minKey;
    unsigned int m_maxKey;
    skiplist_node<V>* m_pHeader;
    skiplist_node<V>* m_pTail;
    std::vector<skiplist_node<V>*> toTail;
};
 
///////////////////////////////////////////////////////////////////////////////
 
#endif // _SKIPLIST_STATIC_H_