/*----------------------------------------------------------------------
  File     : computer.cpp
  Contents : Algorithms for computing and timing top k most similar objects for every object
  Update   : February 2015
----------------------------------------------------------------------*/
#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>
#include "Computer.h"

void readConfigFile(char * &configFileName, std::string& inputFileName, std::string& timingFileName, std::string& controlFileName, unsigned int& k, std::string& algorithm, std::vector<double>& parameterVector){
	std::ifstream configFile(configFileName);
	std::string line;
	unsigned int lineCount = 0;
	while(getline(configFile,line)){
		++lineCount;
		std::string delimiter = ":";
		std::vector<std::string> splitted;
		size_t pos = 0;
		while ((pos = line.find(delimiter)) != std::string::npos) {
		  std::string token;
		  token = line.substr(0, pos);
		  splitted.push_back(token);
		  line.erase(0, pos + delimiter.length());
		}// end of while pos = line.find
		// remaining part of string is also a token
		splitted.push_back(line);
		if(lineCount==1) inputFileName = splitted[0];
		else if(lineCount==2) timingFileName = splitted[0];
		else if(lineCount==3) controlFileName = splitted[0];
		else if(lineCount==4) k = stoi(splitted[0]);
		else if(lineCount==5) algorithm = splitted[0];
		else if(lineCount >5) parameterVector.push_back(stof(splitted[0]));
	}//end of while getline
}//end of readConfigFile

int main(int argc, char *argv[]){
	std::cout << "Algorithms for computing and timing top k most similar objects for every object" << std::endl << std::flush;
	std::cout << "last update: February 2015" << std::endl << std::flush;

	if(argc < 2){
		std::cerr << "usage: " << argv[0] << "config-file-path" << std::endl;
	} else {
		std::cout << "config file path: " << argv[1] << std::endl << std::flush;

		//initalize inputs
		std::string inputFileName;
		std::string timingFileName;
		std::string controlFileName;
		unsigned int k;
		std::string algorithm;
		std::vector<double> parameterVector;
		std::cout << "inputs initialized" << std::endl << std::flush;

		//read inputs from config file
		readConfigFile(argv[1],inputFileName,timingFileName,controlFileName,k,algorithm,parameterVector);
		std::cout << "inputs read from configuration file" << std::endl << std::flush;

		//write info to timing file
		std::ofstream timingFile(timingFileName);
		timingFile << "k," << k << std::endl << std::flush;
		timingFile << "parameter_set_id," << parameterVector[0] << std::endl << std::flush;
		timingFile << "algorithm," << algorithm << std::endl << std::flush;
		timingFile << "inputFileName," << inputFileName << std::endl << std::flush;
		timingFile << "controlFileName," << controlFileName << std::endl << std::flush;
		timingFile.close();

		//create computer object
		Computer computer;
		std::cout << "created computer object" << std::endl << std::flush;

		//set object properties
		computer.setk(k);
		std::cout << "properties of object set" << std::endl << std::flush;

		//execute computation
		std::cout << "start executing computation" << std::endl << std::flush;
		if(algorithm.compare("basic_inverted_index")==0){
			std::cout << "identified algorithm:baseline" << std::endl << std::flush;
			computer.baseline_p(inputFileName,timingFileName,controlFileName);
		} else if(algorithm.compare("naive_baseline")==0){
			std::cout << "identified algorithm:naive_baseline" << std::endl << std::flush;
			computer.non_indexed_baseline(inputFileName,timingFileName,controlFileName);
		} else if(algorithm.compare("dynamicIndex")==0){
			std::cout << "identified algorithm:dynamicIndex_heap" << std::endl << std::flush;
			computer.dynamicIndex_heap(inputFileName,timingFileName,controlFileName);
		} else if(algorithm.compare("prunedIndex")==0){
			std::cout << "identified algorithm:prunedIndex" << std::endl << std::flush;
			// set a default value for the threshold, although it is overwritten by the estimation of the optimal threshold based on a sampled approximation of the cdf
			computer.setThreshold(0.4);
			computer.allNN(inputFileName,timingFileName,controlFileName);
		} else if(algorithm.compare("sc")==0){
			std::cout << "identified algorithm:sc" << std::endl << std::flush;
			computer.sc(inputFileName,timingFileName,controlFileName);
		} else if(algorithm.compare("maxScore")==0){
			std::cout << "identified algorithm:maxScore" << std::endl << std::flush;
			computer.maxScore(inputFileName,timingFileName,controlFileName);
		}  else{
			std::cerr << "ERROR: invalid algorithm name" << std::endl << std::flush;
			std::cout << "executed computation" << std::endl << std::flush; 
		}
	}
	return 0;
}//end of main