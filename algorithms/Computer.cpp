 /*----------------------------------------------------------------------
  File     : computer.cpp
  Contents : compute k-nn graph on sparse high dimensional datasets
  Update   : July 2015
----------------------------------------------------------------------*/
#include <vector> //vector
#include <string> //string
#include <iostream> //cout
#include <iomanip> //setprecision
#include <fstream> //ifstream
#include <tuple> //tuple
#include <algorithm> //nth_element
#include <iterator> //distance
#include <unordered_set>
#include <set>
#include <unordered_map>
#include <list>
#include <math.h>
#include <cmath>
#include <time.h>	
#include <cstring>
#include <cassert> // assert
#include <queue>
#include <map>
#include "Computer.h"
#include "skiplist_static.h"

// A fudge factor so that we are conservative in dealing with floating
// point rounding issues.
const double kFudgeFactor = .00000001;
// const double kFudgeFactor = .0001;

bool compare_pairs_second_bib(std::pair<unsigned int,double> pair1, std::pair<unsigned int,double> pair2){
  return (pair1.second > pair2.second);
}//end of method

bool compare_pairs_second_sib(std::pair<unsigned int,double> pair1, std::pair<unsigned int,double> pair2){
  return (pair1.second < pair2.second);
}//end of method

bool compare_pairs_second_int_sib(std::pair<unsigned int,unsigned int> pair1, std::pair<unsigned int,unsigned int> pair2){
  return (pair1.second < pair2.second);
}//end of method

bool compare_pairs_second_int_bib(std::pair<unsigned int,unsigned int> pair1, std::pair<unsigned int,unsigned int> pair2){
  return (pair1.second < pair2.second);
}//end of method

bool compare_pairs_first_sib(std::pair<unsigned int,double> pair1, std::pair<unsigned int,double> pair2){
  return (pair1.first < pair2.first);
}//end of method

bool compare_triplets_first_sgf(std::tuple<unsigned int,double,double> triplet1, std::tuple<unsigned int,double,double> triplet2){
  return (std::get<0>(triplet1) < std::get<0>(triplet2));
}//end of method

bool compare_triplets_sgf_third(std::tuple<unsigned int,double,double> triplet1, std::tuple<unsigned int,double,double> triplet2){
	return std::get<2>(triplet1) < std::get<2>(triplet2);
}

bool compare_triplets_third_bgf(std::tuple<unsigned int,double,double> triplet1, std::tuple<unsigned int,double,double> triplet2){
	return std::get<2>(triplet1) > std::get<2>(triplet2);
}

void Computer::setk(unsigned int k_in){
	k = k_in;
}//end of setk

void Computer::setm(unsigned int m_in){
	m = m_in;
}//end of setm

void Computer::setThreshold(double t_in){
	threshold = t_in;
	std::cout << "threshold: " << threshold << std::endl << std::flush;
}//end of setm

bool Computer::readObject(std::ifstream &inputFile, std::vector<std::string> &splitted){
	std::string line;
  	splitted.clear();

	if(std::getline(inputFile,line)){
		std::string delimiter = ",";
		size_t pos = 0;
		while ((pos = line.find(delimiter)) != std::string::npos) {
		  std::string token;
		  token = line.substr(0, pos);
		  splitted.push_back(token);
		  line.erase(0, pos + delimiter.length());
		}
		//remaining part of string is also a token. getline removed \n from string.
		splitted.push_back(line);
		return true;
		}
	else return false;

}//end of readObject

void Computer::sc(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm sc" << std::endl << std::flush;
	clock_t start = clock();

	//parameters
	unsigned int N = 2;

	//read data
	std::unordered_map<unsigned int, std::vector<std::pair<unsigned int,double>>> objectsByFeature;
	unsigned int noObjects = 0;
	unsigned int noSegments = 0;

	// std::cout << "c1" << std::endl << std::flush;

	readData(inputFileName,N,objectsByFeature,noObjects,noSegments); //features sorted according to id.

	std::cout<< "noSegments = " << noSegments << std::endl << std::flush;
	std::cout<< "noObjects = " << noObjects << std::endl << std::flush;

	// std::cout << "c2" << std::endl << std::flush;

	//sort the object-lists according to descending weight and segment the object-lists in N(default=8) equal parts and add to segmentsByObject and objectsBySegment
	std::vector<std::vector<std::pair<unsigned int,double>>> segmentsByObject(noObjects);
	std::vector<std::vector<std::pair<unsigned int,double>>> objectsBySegment(noSegments);
	std::vector<double> maxWBySegment(noSegments,0);
	std::unordered_map<unsigned int, std::vector<std::pair<unsigned int,double>>>::iterator listit;
	unsigned int sid = 0; //segment id
	unsigned int lid = 0; //list id
	for(listit=objectsByFeature.begin(); listit!=objectsByFeature.end(); ++listit){

		// std::cout << "c21" << std::endl << std::flush;

		//sort the object-lists according to descending weight
		std::sort(listit->second.begin(),listit->second.end(),compare_pairs_second_bib);	

		// std::cout << "c22" << std::endl << std::flush;

		//segment the object-lists in N(default=8) equal parts and add to segmentsByObject and objectsBySegment
		std::vector<std::pair<unsigned int,double>>::iterator pit;
		unsigned int counter = 0;
		unsigned int counterMax = ceil((double)listit->second.size()/(double)(N));
		// if(listit->second.size()<N) std::cout << "size of list = " << listit->second.size() << std::endl << std::flush;
		// if(counterMax==0) for(pit=listit->second.begin(); pit!=listit->second.end(); ++pit) std::cout << "object = " << pit->first << ", value = " << pit->second << std::endl << std::flush;
 		// std::cout << "counter = " << counter << ", counterMax = " << counterMax << std::endl << std::flush;
		// std::cout << "c23" << std::endl << std::flush;
		unsigned int local_sid = 0;
		sid = lid*(N)+local_sid;
		// std::cout << "c24" << std::endl << std::flush;
		// std::cout << "noSegments = " << noSegments << ", sid = " << sid << std::endl << std::flush;
		maxWBySegment[sid] = ((listit->second).begin())->second; //because of sorting
		// std::cout << "c25" << std::endl << std::flush;
		for(pit=listit->second.begin(); pit!=listit->second.end(); ++pit){
			// std::cout << "lid = " << lid << ", sid = " << sid << std::endl << std::flush;
			objectsBySegment[sid].push_back(*pit);
			// std::cout << "add object " << pit->first << " to segment " << sid << " with value " << pit->second << std::endl << std::flush;
			for(unsigned int n = 0; n<(N); ++n){
				segmentsByObject[pit->first].push_back(std::make_pair(lid*(N)+n,pit->second));
				// std::cout << "add segment " << lid*(N)+n << " to object " << pit->first << " with value " << pit->second << std::endl << std::flush;
			}
			++counter;
			if(counter == counterMax){
				// sort postings in finished segment by document order
				std::sort(objectsBySegment[sid].begin(),objectsBySegment[sid].end(),compare_pairs_first_sib);
				//--------------------------------------------------------------------------------------------
				counter = 0;
				++local_sid;
				sid = lid*(N)+local_sid;
				maxWBySegment[sid] = pit->second; //because of sorting
			}
		}
		std::sort(objectsBySegment[sid].begin(),objectsBySegment[sid].end(),compare_pairs_first_sib);
		// std::cout << "c26" << std::endl << std::flush;
		++lid;
	}

	// std::cout << "c3" << std::endl << std::flush;
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "preproc_time[s]");

	//loop over segmentsByObject
	unsigned int segmentProcessingCounter = 0;
	double avSimPairsSize = 0;
	double avSegmentSize = 0;
	double avNoSkips = 0;
	std::ofstream controlFile(controlFileName);
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int objectId = 0;
	for(objit=segmentsByObject.begin(); objit!=segmentsByObject.end(); ++objit){

		// std::cout << "object ID = " << objectId << std::endl << std::flush;

		// std::cout << "c31" << std::endl << std::flush;
		//construct vector with tuples also containing maxscore.
		std::vector<std::tuple<unsigned int,double,double>> object;
		std::vector<std::pair<unsigned int,double>>::iterator pit;
		double remscore = 0;
		for(pit=objit->begin(); pit!=objit->end(); ++pit){
			double maxscore = maxWBySegment[pit->first]*pit->second;
			object.push_back(std::make_tuple(pit->first,pit->second,maxscore));
			remscore += maxscore;
		}
		// std::cout << "remscore = " << remscore << std::endl << std::flush;
		//sort segments by maxscore.
		std::sort(object.begin(),object.end(),compare_triplets_third_bgf);	
		std::vector<double> remscoreBySegment(noSegments);
		std::vector<std::tuple<unsigned int,double,double>>::reverse_iterator sit;
		double runner = 0;
		for(sit=object.rbegin();sit!=object.rend();++sit){
			runner += std::get<2>(*sit);
			remscoreBySegment[std::get<0>(*sit)] = runner;
		}	

		//OR PHASE
		// std::cout << "c32" << std::endl << std::flush;
		thresholdType threshold;
		for(unsigned int i=0; i<k; ++i) threshold.push(0);
		// std::cout << "threshold = " << threshold.top() << ", size = " << threshold.size() << std::endl << std::flush;
		std::vector<double> scores(noObjects,0);
		std::vector<unsigned int> scoreIndexes;
		std::vector<std::tuple<unsigned int,double,double>>::iterator segit = object.begin();	
		while((segit!=object.end()) && (remscoreBySegment[std::get<0>(*segit)]>=threshold.top()-kFudgeFactor) ){
			// std::cout << "process segment " << std::get<0>(*segit) << " in OR mode." << std::endl << std::flush;
			std::vector<double> deltas(noObjects,0);
			std::vector<unsigned int> deltaIndexes;
			std::vector<std::pair<unsigned int,double>>::iterator it;
			for(it=objectsBySegment[std::get<0>(*segit)].begin(); it!=objectsBySegment[std::get<0>(*segit)].end(); ++it){
				if(deltas[it->first]<kFudgeFactor) deltaIndexes.push_back(it->first);
				deltas[it->first] += it->second*std::get<1>(*segit);
			}
			//remove objectId from deltaIndexes
			deltaIndexes.erase(std::remove(deltaIndexes.begin(), deltaIndexes.end(), objectId), deltaIndexes.end());
			std::vector<unsigned int>::iterator indit;
			for(indit=deltaIndexes.begin(); indit!=deltaIndexes.end(); ++indit){
				if(scores[*indit]<kFudgeFactor) scoreIndexes.push_back(*indit);
				scores[*indit] += deltas[*indit];
				// std::cout << "score of object " << *indit << " is now " << scores[*indit] << std::endl << std::flush;
				if(scores[*indit]>threshold.top()) threshold.pop(),threshold.push(scores[*indit]);
				// std::cout << "threshold = " << threshold.top() << ", size = " << threshold.size() << std::endl << std::flush;
			}
			remscore -= std::get<2>(*segit);
			// std::cout << "remscore = " << remscore << std::endl << std::flush;
			++segit;
		}
		//AND phase
		// std::cout << "c33: start AND phase" << std::endl << std::flush;
		std::sort(scoreIndexes.begin(),scoreIndexes.end());
		std::vector<std::pair<unsigned int,double>> simPairs;
		std::vector<unsigned int>::iterator indit;
		for(indit=scoreIndexes.begin(); indit!=scoreIndexes.end(); ++indit){
			simPairs.push_back(std::make_pair(*indit,scores[*indit]));
		}
		// std::cout << "original size of simPairs = " << simPairs.size() << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		// if(objectId==1180)for(simpit=simPairs.begin();simpit!=simPairs.end();++simpit) if((simpit->first==4276) || (simpit->first==4168)) std::cout << "object: " << simpit->first << ", similarity: " << simpit->second << std::endl << std::flush;
		while((segit!=object.end()) && (simPairs.size()>k)){
			// std::cout << "in while, segment = " << std::get<0>(*segit) << std::endl << std::flush;
			std::vector<std::pair<unsigned int,double>>::iterator spit = objectsBySegment[std::get<0>(*segit)].begin();
			std::vector<std::pair<unsigned int,double>>::iterator acit = simPairs.begin();
			while( (spit!=objectsBySegment[std::get<0>(*segit)].end()) && (acit!=simPairs.end()) ){
				// std::cout << "accumulator iterator points to " << acit->first << ", postings iterator points to " << spit->first << std::endl << std::flush;
				if(spit->first<acit->first){
					++spit;
					// skipTo(spit,skipit,acit->first,objectsBySegment[std::get<0>(*segit)]);
				} else if(acit->first<spit->first){
					++acit;
				} else {
					acit->second += spit->second*std::get<1>(*segit); 
					// std::cout << "c3301" << std::endl << std::flush;
					if(acit->second>threshold.top()){
						// std::cout << "c33010" << std::endl << std::flush;
						threshold.pop();
						// std::cout << "c33011" << std::endl << std::flush;
						threshold.push(acit->second);
						// std::cout << "c33012" << std::endl << std::flush;
					}
					// std::cout << "c3302" << std::endl << std::flush;
					++spit; 
					++acit;
				} 
			}

			//statistics
			avSegmentSize = (double)(avSegmentSize*segmentProcessingCounter + objectsBySegment[std::get<0>(*segit)].size())/(double)(segmentProcessingCounter+1);
			avSimPairsSize = (double)(avSimPairsSize*segmentProcessingCounter + simPairs.size())/(double)(segmentProcessingCounter+1);
			avNoSkips = (double)(avNoSkips*segmentProcessingCounter + sqrt(simPairs.size()*objectsBySegment[std::get<0>(*segit)].size())/2.0)/(double)(segmentProcessingCounter+1);
			++segmentProcessingCounter;

			// std::cout << "c331" << std::endl << std::flush;
			remscore -= std::get<2>(*segit);
			// std::cout << "remscore = " << remscore << ", threshold = " << threshold.top() << std::endl << std::flush;
			//trim accumulators
			acit=simPairs.begin();
			while(acit!=simPairs.end()){
				// std::cout << "candidate = " << acit->first << ", temp score = " << acit->second << ", remscore = " << remscore << ", threshold = " << threshold.top() << std::endl << std::flush;
				if(acit->second+remscoreBySegment[std::get<0>(*segit)]+kFudgeFactor < threshold.top()) acit = simPairs.erase(acit);
				else ++acit;
			}
			++segit;
		}
		// if(objectId==1494)for(simpit=simPairs.begin();simpit!=simPairs.end();++simpit) if((simpit->first==584) || (simpit->first==3714)) std::cout << "object: " << simpit->first << ", similarity: " << simpit->second << std::endl << std::flush;
		// std::cout << "c34" << std::endl << std::flush;
		// std::cout << "c35" << std::endl << std::flush;
		// std:: cout << "size of simPairs = " << simPairs.size() << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>> topk;
		if(simPairs.size()>=k){
			std::nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
			std::copy(simPairs.begin(), simPairs.begin()+k,std::back_inserter(topk));
			double th = topk[k-1].second;
			for(simpit=simPairs.begin()+k; simpit!=simPairs.end(); ++simpit) if(simpit->second >= th-kFudgeFactor) topk.push_back(*simpit);
		} else {
			topk = simPairs;
		}
		// std::cout << "c36" << std::endl << std::flush;
		
		// std::cout << "c37" << std::endl << std::flush;
		// std::cout << "final size of simPairs = " << simPairs.size() << std::endl << std::flush;
		
		// for(simpit=simPairs.begin();simpit!=simPairs.end();++simpit) std::cout << "object: " << simpit->first << ", similarity: " << simpit->second << std::endl << std::flush;
		// std::cout << "c38" << std::endl << std::flush;
		writeTopk(controlFile,objectId,topk);
		// std::cout << "c39" << std::endl << std::flush;
		++objectId;
		// std::cout << "c310" << std::endl << std::flush;
	}
	std::cout << "average segment size = " << avSegmentSize << ", average accumulators size = " << avSimPairsSize << ", average number of skip pointers = " << avNoSkips << std::endl << std::flush;
	// std::cout << "c4" << std::endl << std::flush;
	controlFile.close();
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
}//end of sc

// void Computer::skipTo(std::vector<std::pair<unsigned int,double>>::iterator& spit, std::vector<std::pair<unsigned int, unsigned int>>::iterator& skipit, unsigned int acid, std::vector<std::pair<unsigned int,unsigned int>>& postings){
// 	while((skipit+1)->first<acid) ++skipit;
// 	spit = postings.begin()+skipit->second;
// 	while(spit->first<acid) ++spit;

// }//end of skipTo

void Computer::writeTopk(std::ofstream& controlFile, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& topk){
	// current method takes n log k i.s.o n time complexity;
	controlFile << targetIndex;
	while(topk.size()>0){
		controlFile << "," << topk.back().first;
		topk.pop_back();
	}
	controlFile << std::endl << std::flush;
	
}//end of writeTopks

void Computer::readData(std::string inputFileName, unsigned int N, std::unordered_map<unsigned int, std::vector<std::pair<unsigned int,double>>>& objectsByFeature, unsigned int& noObjects, unsigned int& noSegments){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int objectId = 0;
	std::unordered_set<unsigned int> features;
	// std::cout << "c11" << std::endl << std::flush;
	while(readObject(inputFile,object)){
		// std::cout << "c111" << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>> formattedObject;
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		double sqnorm = 0;
		// std::cout << "c112" << std::endl << std::flush;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				features.insert(key);
				isKey = false;
			} else {
				val = atof(featit->c_str());
				formattedObject.push_back(std::make_pair(key,val));
				sqnorm += pow(val,2);
				isKey = true;
			}
		}
		// std::cout << "c113" << std::endl << std::flush;
		double norm = sqrt(sqnorm);
		// std::cout << "c114" << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator it;
		for(it=formattedObject.begin(); it!=formattedObject.end(); ++it){
			objectsByFeature[it->first].push_back(std::make_pair(objectId,it->second/norm));
		}
		// std::cout << "c115" << std::endl << std::flush;
		++objectId;
	}
	// std::cout << "c12" << std::endl << std::flush;
	noObjects = objectId;
	noSegments = features.size()*(N+1);
	inputFile.close();
}//end of readObjects

void Computer::non_indexed_baseline(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm non_indexed_baseline" << std::endl << std::flush;
	clock_t start = clock();

	std::vector<std::vector<std::pair<unsigned int,double>>> objects;
	readObjects(inputFileName,objects); //features sorted according to id.

	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int objectId = 0;
	std::ofstream controlFile(controlFileName);
	for(objit=objects.begin(); objit!=objects.end(); ++objit){
		// std::cout << "target = " << objectId << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>> simPairs;
		std::vector<std::vector<std::pair<unsigned int,double>>>::iterator candit;
		unsigned int candidateId = 0;
		for(candit=objects.begin(); candit!=objects.end(); ++candit){
			if(objectId!=candidateId){
				double score = dot(*objit,*candit);
				if(score>kFudgeFactor){
					simPairs.push_back(std::make_pair(candidateId,score));
				}
			}
			++candidateId;
		}
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		// for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		// 	std::cout << "\t candidate: " << simpit->first << ", score: " << simpit->second << std::endl << std::flush;
		// }
		std::list<unsigned int> topk;
		extractTopk(simPairs,topk);
		writeTopk(objectId,topk,controlFile);
		++objectId;
	}
	controlFile.close();
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
}//end of non_indexed_baseline

void Computer::readObjects(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& objects){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	while(readObject(inputFile,object)){
		std::vector<std::pair<unsigned int,double>> formattedObject;
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		double sqnorm = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str());
				formattedObject.push_back(std::make_pair(key,val));
				sqnorm += pow(val,2);
				isKey = true;
			}
		}
		sort(formattedObject.begin(),formattedObject.end(),compare_pairs_first_sib);
		double norm = sqrt(sqnorm);
		std::vector<std::pair<unsigned int,double>>::iterator it;
		for(it=formattedObject.begin(); it!=formattedObject.end(); ++it) it->second/=norm;
		objects.push_back(formattedObject);
	}
	inputFile.close();
}//end of readObjects

void Computer::skipToObject(postingIteratorType& pit, postingIteratorType endOfPosting, unsigned int objectId, bool& isMatch, bool& isFinished){
	while((pit!=endOfPosting) && (pit->first < objectId)){
		++pit;
	}
	if(pit!=endOfPosting){
		if(pit->first == objectId){
			isMatch = true;
		}
	} else {
		isFinished = true;
	}
}//end of skipToObject

void Computer::readObjects(std::string inputFileName, std::vector<double>& normByObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int objectId = 0;
	while(readObject(inputFile,object)){
		std::vector<std::pair<unsigned int,double>>* op = &objects[objectId];
		std::vector<std::string>::iterator featit;
		size_t size = 0;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str())/normByObjectId[objectId];
				op->push_back(std::make_pair(key,val));
				objectsByFeature[key].push_back(std::make_pair(objectId,val));
				++size;
				isKey = true;
			}
		}
		++objectId;
	}
	inputFile.close();
}//end of readObjects

void Computer::extractGlobalStats_mw(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId){

	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	noObjects = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		std::vector<std::pair<unsigned int,double>> temp;
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str());
				sqnorm += pow(val,2.0);
				temp.push_back(std::make_pair(key,val));
				isKey = true;
			}
		}
		double norm = sqrt(sqnorm);
		normByOldObjectId.push_back(norm);
		std::vector<std::pair<unsigned int,double>>::iterator pit;
		double normalized = 0;
		for(pit=temp.begin(); pit!=temp.end(); ++pit){
			normalized = pit->second/norm;
			maxWByOldObjectId[noObjects] = std::max(normalized,maxWByOldObjectId[noObjects]);
			maxWByOldFeatureId[pit->first] = std::max(normalized,maxWByOldFeatureId[pit->first]);
		}
		++noObjects;
	}
	inputFile.close();
	noFeatures = maxWByOldFeatureId.size();
	std::unordered_map<unsigned int,double>::iterator upit;
	for(upit=maxWByOldFeatureId.begin(); upit!=maxWByOldFeatureId.end(); ++upit) featureValPairs.push_back(*upit);
	std::unordered_map<unsigned int,double>::iterator vpit;
	for(vpit=maxWByOldObjectId.begin(); vpit!=maxWByOldObjectId.end(); ++vpit) objectValPairs.push_back(*vpit);
}//end of extractGlobalStats

void Computer::maxScore(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm maxScore" << std::endl << std::flush;
	clock_t start = clock();
	//first pass over data to count number of objects and features and compute object norms
	std::tuple<unsigned int,unsigned int,std::vector<double>,std::vector<unsigned int>> globalInfo; //#objects, #features
	globalInfo = getGlobalInfoPlus(inputFileName);
	unsigned int noObjects = std::get<0>(globalInfo);
	unsigned int noFeatures = std::get<1>(globalInfo);
	std::vector<double> norms = std::get<2>(globalInfo);
	std::vector<unsigned int> countByFeature = std::get<3>(globalInfo);
	std::cout << "extracted global info in first pass over data" << std::endl << std::flush;


	//initialize inverted index of correct size
	std::vector<skiplist_static<double>*> objectsByFeature (noFeatures);
	std::vector<skiplist_static<double>*>::iterator vit;
	unsigned int featureIndex = 0;
	for(vit=objectsByFeature.begin(); vit!=objectsByFeature.end(); ++vit){
		*vit = new skiplist_static<double>(0,noObjects-1,1+floor(log2(countByFeature[featureIndex])),featureIndex);//create version with special comparison function if no primitive value type like double, e.g. pair
		++featureIndex;
	}
	//fill inverted index during second pass over data
	fillSkiplists(inputFileName, objectsByFeature, norms);
	std::cout << "filled inverted index" << std::endl << std::flush;

	//write timing
	clock_t preprocTimePoint = clock();
	writeTiming(timingFileName, double(preprocTimePoint-start)/double(CLOCKS_PER_SEC), "preproc_time[s]");

	//for every document
	std::cout << "start loop over inputFile" << std::endl << std::flush;
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::ofstream controlFile(controlFileName);
	std::vector<std::string> object;
	unsigned int targetIndex = 0;
	while(readObject(inputFile,object)){

		// std::cout << "targetIndex = " << targetIndex << std::endl << std::flush;

		//when document is read (again) to compute similarities, sort feature iterator according to maxscore
		std::vector<std::pair<unsigned int,double>> targetFeatures;
		std::vector<skiplist_static<double>*>* pObjectsByFeature = &objectsByFeature;
		DescendingMaxScore comparator(pObjectsByFeature);
		processObject(targetIndex,object,targetFeatures,norms,comparator);
		// std::cout << "after processObject" << std::endl << std::flush;

		//initialize q_req and topk priority queue positionMap
		unsigned int q = targetFeatures.size();
		unsigned int q_req = q;
		std::vector<std::pair<unsigned int,double>> temp(k);
		for(unsigned int i=0; i<k; ++i) temp.push_back(std::make_pair(targetIndex,minSim));
		topkType topk(temp.begin(),temp.end());
		std::pair<unsigned int,double> topk_meta(k,minSim);
		std::vector<unsigned int> pos(q);//introduce pos because iterators i will need to be deleted and in this way it needs to be done for only one light vector i.s.o. many light vectors or one heavy (i.e. with big tuples in it) vectors
		std::vector<unsigned int> index(q);//inverse of pos
		for(unsigned int i = 0; i<q; ++i) pos[i] = i, index[i] = i;
		// std::cout << "after init topk" << std::endl << std::flush;

		//compute cumulative scores
		std::vector<double> cumulScores(q);
		// std::cout << "after preproc init cumulScores" << std::endl << std::flush;
		cumulScores[q-1] = targetFeatures[q-1].second*objectsByFeature[targetFeatures[q-1].first]->maxScore;
		// std::cout << "after init cumulScores first element" << std::endl << std::flush;
		for(unsigned int j=0; j<q-1; ++j){
			unsigned int i = q-2-j;
			cumulScores[i] = cumulScores[i+1]+targetFeatures[i].second*objectsByFeature[targetFeatures[i].first]->maxScore;
		}
		// std::cout << "after init cumulScores" << std::endl << std::flush;

		//initialize skiplist pointers
		std::vector<skiplist_node<double>*> slNodePointers(q);
		std::vector<unsigned int> pointedObjects(q);
		for(unsigned int i=0; i<q; ++i){
			slNodePointers[i] = objectsByFeature[targetFeatures[i].first]->getStart();
			pointedObjects[i] = slNodePointers[i]->getKey();
		}

		// std::cout << "after init skipLIst pointers" << std::endl << std::flush;

		//while over iterators
		while((q>0) && (q_req>0)){
			// std::cout << "new iteration of while" << std::endl << std::flush;

			//intialize candidate score to zero
			double score = 0;
			bool changedMinScore = false;

			//select candidate object and it's positions
			unsigned int cand = noObjects;
			std::vector<unsigned int> candPos;
			for(unsigned int i=0; i<q_req; ++i){
				if(pointedObjects[pos[i]]<=cand){
					if(pointedObjects[pos[i]]<cand){
						cand = pointedObjects[pos[i]];
						candPos.clear();
						candPos.push_back(pos[i]);
					} else{
						//==cand
						candPos.push_back(pos[i]);
					}
				}
			}

			std::vector<unsigned int>::iterator candit;
			if(cand==targetIndex){
				for(candit=candPos.begin(); candit!=candPos.end(); ++candit){
					if(objectsByFeature[targetFeatures[*candit].first]->advance(slNodePointers[*candit])==false){
						// std::cout << "will delete iterator" << std::endl << std::flush;
						for(unsigned int j = 0; j<index[*candit]; ++j) cumulScores[pos[j]] -= targetFeatures[*candit].second*objectsByFeature[targetFeatures[*candit].first]->maxScore;
						for(unsigned int j = index[*candit]; j<q-1; ++j) pos[j] = pos[j+1], index[pos[j]] = j;
						pos.pop_back();
						--q;
						--q_req;
					} else {
						// std::cout << "update values pointed at" << std::endl << std::flush;
						pointedObjects[*candit] = slNodePointers[*candit]->getKey();
					}
				}
				goto itWhile;
			}

			for(candit=candPos.begin(); candit!=candPos.end(); ++candit){
				// std::cout << "feature = " << targetFeatures[*candit].first << std::endl << std::flush;
				score += slNodePointers[*candit]->value*targetFeatures[*candit].second;
				// std::cout << "score = " << score << std::endl << std::flush;
				// std::cout << "after upate score" << std::endl << std::flush;
				if(objectsByFeature[targetFeatures[*candit].first]->advance(slNodePointers[*candit])==false){
					// std::cout << "will delete iterator" << std::endl << std::flush;
					for(unsigned int j = 0; j<index[*candit]; ++j) cumulScores[pos[j]] -= targetFeatures[*candit].second*objectsByFeature[targetFeatures[*candit].first]->maxScore;
					for(unsigned int j = index[*candit]; j<q-1; ++j) pos[j] = pos[j+1], index[pos[j]] = j;
					pos.pop_back();
					--q;
					--q_req;
				} else {
					// std::cout << "update values pointed at" << std::endl << std::flush;
					pointedObjects[*candit] = slNodePointers[*candit]->getKey();
				}
			}	
			// std::cout << "after first score updating" << std::endl << std::flush;	
			// std::cout << "q = " << q << std::endl << std::flush;		
			// std::cout << "q_req = " << q_req << std::endl << std::flush;	

			for(unsigned int i = q_req; i<q; ++i){

				if (score + cumulScores[pos[i]] < topk_meta.second) goto itWhile;
				
				if(objectsByFeature[targetFeatures[pos[i]].first]->skipTo(slNodePointers[pos[i]],cand)==false){
					// std::cout << "skipTo=false"<< std::endl << std::flush;
					// std::cout << "i = " << i << std::endl << std::flush;
					// std::cout << "pos[i] = " << pos[i] << std::endl << std::flush;
					for(unsigned int j = 0; j<i; ++j) cumulScores[pos[j]] -= targetFeatures[pos[i]].second*objectsByFeature[targetFeatures[pos[i]].first]->maxScore;
					for(unsigned int j = i; j<q-1; ++j) pos[j] = pos[j+1], index[pos[j]]=j;
					pos.pop_back();
					--q;
					// std::cout << "q = " << q << std::endl << std::flush;
					--i; //to take into account the deletion of the current i and the ++i after the continue statement
					continue;
				}
				// std::cout << "skipTo=true"<< std::endl << std::flush;
				// std::cout << "did not continue" << std::endl << std::flush;
				if(slNodePointers[pos[i]]->getKey()==cand){
					score += slNodePointers[pos[i]]->value*targetFeatures[pos[i]].second;
					// std::cout << "score = " << score << std::endl << std::flush;

				}
				// std::cout << "will iterate for" << std::endl << std::flush;
			}
			// std::cout << "q_req = " << q_req << std::endl << std::flush;

			updateTopk(topk,cand,score,topk_meta,changedMinScore);
			if(changedMinScore){
				int i = q_req-1;
				while(q_req>0 && cumulScores[pos[i]]<topk_meta.second){
					// std::cout << "i = " << i << std::endl << std::flush;
					// std::cout << "pos[i] = " << pos[i] << std::endl << std::flush;
					// std::cout << "cumulScore = " << cumulScores[pos[i]] << std::endl << std::flush;
					--q_req;//for memory efficient version: If no skip structure yet, build skip structure on them.
					--i;
					// std::cout << "q_req = " << q_req << std::endl << std::flush;
				}
			} 
			// std::cout << "q_req = " << q_req << std::endl << std::flush;

			itWhile:;
		}
		// std::cout << "after while q>0" << std::endl << std::flush;
		writeTopk(controlFile,topk,targetIndex);
		++targetIndex;
	}
	std::cout << "before closing inputFile and controlFile" << std::endl << std::flush;
	inputFile.close();
	controlFile.close();
	std::cout << "after closing inputFile and controlFile" << std::endl << std::flush;
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
}//end of maxScore

void Computer::writeTopk(std::ofstream& controlFile, topkType& topk, unsigned int targetIndex){
	// current method takes n log k i.s.o n time complexity;
	controlFile << targetIndex;
	while(topk.size()>0){
		controlFile << "," << topk.top().first;
		topk.pop();
	}
	controlFile << std::endl << std::flush;
	
}//end of writeTopks

void Computer::updateTopk(topkType& topk, unsigned int candidate, double similarity , std::pair<unsigned int,double>& inTailSize, bool& changedMinScore){
	//make special case for k=1? choose between different function binders higher up
	if(similarity>=inTailSize.second-kFudgeFactor){
		if(similarity>inTailSize.second+kFudgeFactor){
			if(inTailSize.first == 1){
				changedMinScore = true;
				do{
					topk.pop();
				} while(topk.size()>=k);
				topk.push(std::make_pair(candidate,similarity));
				inTailSize.second = topk.top().second;
				resetTopk(topk,inTailSize);
			} else {
				--inTailSize.first;
				topk.push(std::make_pair(candidate,similarity));
			}

		} else if(similarity>inTailSize.second){
			if(inTailSize.first == 1){
				topk.push(std::make_pair(candidate,similarity));
				inTailSize.second = similarity;
				while(topk.top().second<inTailSize.second-kFudgeFactor){
					topk.pop();
				}
			} else {
				handleNewMin(topk,inTailSize);
			}
		} else {
			topk.push(std::make_pair(candidate,similarity));
			inTailSize.second = similarity;
		}
	}
}//end of updateTopk

void Computer::processObject(unsigned int objectIndex, std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<double>& norms, DescendingMaxScore comparator){
	//object = [key,val,key,val, ... , key,val]
	std::vector<std::string>::iterator featit;
	bool isKey = true;
	unsigned int key = 0;
	double val = 0; 
	for(featit=object.begin(); featit!=object.end(); ++featit){
		if(isKey){
			key = atoi(featit->c_str()); 
			isKey = false;
		}
		else{
			val = atof(featit->c_str()); 
			targetFeatures.push_back(std::make_pair(key,val/norms[objectIndex]));
			isKey = true;
		}
	} //end of for
	sort(targetFeatures.begin(),targetFeatures.end(),comparator);
}//end of processObject

void Computer::fillSkiplists(std::string inputFileName, std::vector<skiplist_static<double>*>& objectsByFeature,std::vector<double>& norms){
	std::cout << "start to fill inverted index" << std::endl << std::flush;
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0; 
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str()); 
				isKey = false;
			}
			else{
				val = atof(featit->c_str()); 
				objectsByFeature[key]->push_back(objectIndex,val/norms[objectIndex]);
				isKey = true;
			}
		} //end of for
		++ objectIndex;
	}//end of while
	inputFile.close();
}//end of buildInvertedIndices

std::tuple<unsigned int,unsigned int,std::vector<double>,std::vector<unsigned int>> Computer::getGlobalInfoPlus(std::string inputFileName){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	std::unordered_set<unsigned int> features;
	std::vector<double> norms;
	std::unordered_map<unsigned int,unsigned int> countByFeatureMap;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				unsigned int featureId = atoi(featit->c_str());
				features.insert(featureId);
				countByFeatureMap[featureId] += 1;
				isKey = false;
			} else {
				sqnorm += pow(atof(featit->c_str()),2.0);
				isKey = true;
			}
		} //end of for
		norms.push_back(sqrt(sqnorm));
		++objectIndex;
	}//end of while
	inputFile.close();
	//transform map to vector
	std::vector<unsigned int> countByFeature(features.size());
	std::unordered_map<unsigned int,unsigned int>::iterator pit;
	for(pit=countByFeatureMap.begin();pit!=countByFeatureMap.end();++pit) countByFeature[pit->first] = pit->second;
	return std::make_tuple(objectIndex,features.size(),norms,countByFeature);
}//end of count

void Computer::readData(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, unsigned int& noFeatures, std::vector<double>& norms, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& maxValByNewFeatureId, std::vector<double>& maxValByNewObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId){
	std::unordered_map<unsigned int,unsigned int> featureCount;

	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		unsigned int newObjectId = newByOldObjectId[oldObjectId];
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		std::vector<std::string>::iterator featit;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str()); 
				featureCount[key]+=1;
				isKey = false;
			}
			else{
				val = atof(featit->c_str()); 
				objects[newObjectId].push_back(std::make_pair(key,val));
				norms[newObjectId] += pow(val,2);
				isKey = true;
			}
		} //end of for
		norms[newObjectId] = sqrt(norms[newObjectId]);
		++oldObjectId;
	}//end of loop over lines
	noFeatures = featureCount.size();
	std::vector<std::pair<unsigned int,unsigned int>> featureCountPairs;
	std::unordered_map<unsigned int,unsigned int>::iterator pit;
	for(pit=featureCount.begin();pit!=featureCount.end();++pit) featureCountPairs.push_back(*pit);
	newByOldFeatureId.resize(noFeatures,0);
	oldByNewFeatureId.resize(noFeatures,0);
	mapFeatures(featureCountPairs,newByOldFeatureId,oldByNewFeatureId);
	maxValByNewFeatureId.resize(noFeatures,0);
	reCodeFeatures(objects,norms,newByOldFeatureId,maxValByNewObjectId,maxValByNewFeatureId);;
	// normalize(objects[newObjectId],norms,maxValByNewObjectId,maxValByOldFeatureId);

}//end of readData

void Computer::reCodeFeatures(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& norms, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& maxValByNewObjectId, std::vector<double>& maxValByNewFeatureId){
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int newObjectId = 0;
	for(objit=objects.begin(); objit!=objects.end(); ++objit){
		std::vector<std::pair<unsigned int,double>> recodedObject;
		std::vector<std::pair<unsigned int,double>>::iterator featit;
		for(featit=objit->begin(); featit!=objit->end(); ++featit){
			unsigned int newFeatureId = newByOldFeatureId[featit->first];
			double val = featit->second/norms[newObjectId];
			recodedObject.push_back(std::make_pair(newFeatureId,val));
			maxValByNewObjectId[newObjectId] = std::max(maxValByNewObjectId[newObjectId],val);
			maxValByNewFeatureId[newFeatureId] = std::max(maxValByNewFeatureId[newFeatureId],val);
		}
		std::sort(recodedObject.begin(),recodedObject.end(),compare_pairs_first_sib);
		*objit = recodedObject;
		++newObjectId;
	}
}//end of reCodeFeatures

void Computer::mapFeatures(std::vector<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId){
	std::sort(featureCountPairs.begin(),featureCountPairs.end(),compare_pairs_second_int_bib);
	std::vector<std::pair<unsigned int, unsigned int>>::iterator pit;
	unsigned int newFeatureId = 0;
	for(pit=featureCountPairs.begin(); pit!=featureCountPairs.end(); ++pit){
		oldByNewFeatureId[newFeatureId] = pit->first;
		newByOldFeatureId[pit->first] = newFeatureId;
		++newFeatureId;
	}

}//end of mapFeatures

void Computer::mapObjects(std::vector<std::pair<unsigned int, double>>& objectFloorPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId){
	std::sort(objectFloorPairs.begin(),objectFloorPairs.end(),compare_pairs_second_sib);
	std::vector<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newObjectId = 0;
	for(pit=objectFloorPairs.begin(); pit!=objectFloorPairs.end(); ++pit){
		oldByNewObjectId[newObjectId] = pit->first;
		newByOldObjectId[pit->first] = newObjectId;
		++newObjectId;
	}

}//end of mapObjects

void Computer::addToIndex_sorted(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal){
	// std::cout << "in add to index" << std::endl << std::flush;
	double b = 0;
	double checkb = 0;
	std::vector<double> bs;
	unsigned int count = 1;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = targetFeatures.begin();
	std::vector<double> vals;
	while((stayit!=targetFeatures.end()) && (b<minSim-kFudgeFactor)){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		bs.push_back(b);
		// if(targetIndex==650){
			// std::cout << "feature = " << stayit->first << ", val = " << stayit->second << ", maxVal = " << maxValByNewFeatureId[stayit->first] << ", b = " << b << std::endl << std::flush;
		// }
		vals.push_back(stayit->second);
		++stayit;
		++count;
	}
	checkb = b;
	if(vals.empty()){
		maxVal = 0;
	} else {
		vals.pop_back();
		maxVal = *std::max_element(vals.begin(),vals.end());
	}

	while(stayit!=targetFeatures.end()){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		bs.push_back(b);
		++stayit;
	}

	if(checkb>=minSim-kFudgeFactor){
		// std::cout << "will add because checkb = " << checkb << " minSim-kFudgeFactor = " << minSim-kFudgeFactor << std::endl << std::flush;
		count -= 2;
		unsigned int size = targetFeatures.size();
		while(size>count){
			std::vector<std::tuple<unsigned int,double,double>>::iterator insertit;
			std::tuple<unsigned int,double,double> triplet(targetIndex,targetFeatures.back().second,bs.back());
			insertit = std::lower_bound(objectsByFeature[targetFeatures.back().first].begin(),objectsByFeature[targetFeatures.back().first].end(),triplet,compare_triplets_sgf_third);
			objectsByFeature[targetFeatures.back().first].insert(insertit,triplet);
			// objectsByFeature[targetFeatures.back().first].push_back(std::make_tuple(targetIndex,targetFeatures.back().second,bs.back()));
			// if(targetIndex==650){
				// std::cout << "pusched "<< targetIndex << " to feature = " << targetFeatures.back().first << ", b = " << b << std::endl << std::flush;
			// }
			targetFeatures.pop_back();
			bs.pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal){
	// std::cout << "in add to index" << std::endl << std::flush;
	double b = 0;
	double checkb = 0;
	std::vector<double> bs;
	unsigned int count = 1;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = targetFeatures.begin();
	std::vector<double> vals;
	while((stayit!=targetFeatures.end()) && (b<minSim-kFudgeFactor)){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		bs.push_back(b);
		// if(targetIndex==650){
			// std::cout << "feature = " << stayit->first << ", b = " << b << std::endl << std::flush;
		// }
		vals.push_back(stayit->second);
		++stayit;
		++count;
	}
	checkb = b;
	if(vals.empty()){
		maxVal = 0;
	} else {
		vals.pop_back();
		maxVal = *std::max_element(vals.begin(),vals.end());
	}

	while(stayit!=targetFeatures.end()){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		bs.push_back(b);
		++stayit;
	}
	if(checkb>=minSim-kFudgeFactor){
		count -= 2;
		unsigned int size = targetFeatures.size();
		while(size>count){
			objectsByFeature[targetFeatures.back().first].push_back(std::make_tuple(targetIndex,targetFeatures.back().second,bs.back()));
			// if(targetIndex==650){
				// std::cout << "pusched 650 to feature = " << targetFeatures.back().first << ", b = " << b << std::endl << std::flush;
			// }
			targetFeatures.pop_back();
			bs.pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<unsigned int>& offsetByFeature, std::vector<unsigned int>& newByOldObjectId){
	// std::cout << "in compute similarities" << std::endl << std::flush;
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		// std::cout << "before advancing start iterator of feature " << featit->first << std::endl << std::flush;
		// std::cout << "number of features in index = " << objectsByFeature.size() << std::endl <<std::flush;
		// std::cout << "size of feature list = " << objectsByFeature[featit->first].size() << std::endl << std::flush;
		updateOffset(objectsByFeature, offsetByFeature[featit->first],minSim,objects,objectsByFeature[featit->first].size(),featit->first,newByOldObjectId);
		// std::cout << "after advancing start iterator of feature " << featit->first << std::endl << std::flush;
		std::vector<std::tuple<unsigned int,double,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin()+offsetByFeature[featit->first]; candit!=objectsByFeature[featit->first].end(); ++candit){
			// std::cout << "in loop over remaining features" << std::endl << std::flush;
			// std::cout << "candidate: " << std::get<0>(*candit) << "value: " << std::get<1>(*candit) << std::endl << std::flush; 
			if(similarities[std::get<0>(*candit)]==0) linkedObjects.push_back(std::get<0>(*candit));
			similarities[std::get<0>(*candit)] += (featit->second)*(std::get<1>(*candit));
		}
	}
	// std::cout << "after loop over remaining features" << std::endl << std::flush;
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::updateOffset(std::vector<std::vector<std::tuple<unsigned int,double,double>>>& followersByFeature, unsigned int& offset, double minSim, std::vector<std::vector<std::pair<unsigned int, double>>>& objects, unsigned int size, unsigned int featureId, std::vector<unsigned int>& newByOldObjectId){
	// std::cout << "in update iterator" << std::endl << std::flush;
	// std::cout << "7: list of feature 1264 is empty?" << followersByFeature[1264].empty() << std::endl << std::flush;
	// std::cout << "7: size of list of feature 1264 is " << followersByFeature[1264].size() << std::endl << std::flush;

	std::vector<std::tuple<unsigned int,double,double>>::iterator begin = followersByFeature[featureId].begin();
	bool cont = true;
	while(cont){
		if(offset==size){
			// std::cout << "8: list of feature 1264 is empty?" << followersByFeature[1264].empty() << std::endl << std::flush;
			// std::cout << "8: size of list of feature 1264 is " << followersByFeature[1264].size() << std::endl << std::flush;

			// std::cout << "end of list" << std::endl << std::flush;
			cont=false;
		} else if(std::get<2>(*(begin+offset)) < minSim){	
			// std::cout << "9: list of feature 1264 is empty?" << followersByFeature[1264].empty() << std::endl << std::flush;
			// std::cout << "9: size of list of feature 1264 is " << followersByFeature[1264].size() << std::endl << std::flush;

			// std::cout << "now -removing- object " << std::get<0>(*(begin+offset)) << " from list of feature " << featureId << std::endl << std::flush;
			objects[newByOldObjectId[std::get<0>(*(begin+offset))]].push_back(std::make_pair(featureId,std::get<1>(*(begin+offset)))); //can push_back because only for completion of similarities now.
			
			// if(std::get<0>(*(begin+offset))==650){
			// 	std::cout << "removed object 650 from feature = " << featureId << ", b = " << std::get<2>(*(begin+offset)) << std::endl << std::flush;
			// }
			
			++offset;

			// std::cout << "10: list of feature 1264 is empty?" << followersByFeature[1264].empty() << std::endl << std::flush;
			// std::cout << "10: size of list of feature 1264 is " << followersByFeature[1264].size() << std::endl << std::flush;
		} else {
			// std::cout << "11: list of feature 1264 is empty?" << followersByFeature[1264].empty() << std::endl << std::flush;
			// std::cout << "11: size of list of feature 1264 is " << followersByFeature[1264].size() << std::endl << std::flush;
			// std::cout << "do not progress" << std::endl << std::flush;
			cont=false;
			// std::cout << "next object = " << std::get<0>(*(begin+offset)) << " from list of feature" << std::endl << std::flush;
		}
	}
}//end of updateStartIterator

void Computer::updateTopk(topkType& topk, std::vector<std::pair<unsigned int,double>>& simPairs, std::pair<unsigned int,double>& inTailSize){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		updateTopk(topk,simpit->first,simpit->second,inTailSize,0);//0 is dummy
	}
}

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, unsigned int newIndex, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewObjectId, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, unsigned int noFeatures){
	std::vector<std::pair<unsigned int,double>> targetObject = objects[newIndex];
	size_t targetSize = targetObject.size();
	double targetMaxVal = maxValByNewObjectId[newIndex];
	double targetMinSim = inTailSizeByObject[targetIndex].second;
	std::vector<std::pair<unsigned int,double>>::iterator simpit;

	std::vector<double> denseTarget(noFeatures,0);
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetObject.begin(); featit!=targetObject.end(); ++featit){
		denseTarget[featit->first]=featit->second;
	}

	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		unsigned int newCandId = newByOldObjectId[simpit->first];
		if(simpit->second+std::min(targetSize,objects[newCandId].size())*maxValByNewObjectId[newCandId]*targetMaxVal>=std::min(inTailSizeByObject[simpit->first].second,targetMinSim)-kFudgeFactor){
			simpit->second  += half_dense_dot(objects[newCandId],denseTarget);
		}
	}
}//end of completeSimliarities

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, unsigned int newIndex, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewObjectId, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject){
	std::vector<std::pair<unsigned int,double>> targetObject = objects[newIndex];
	size_t targetSize = targetObject.size();
	double targetMaxVal = maxValByNewObjectId[newIndex];
	double targetMinSim = inTailSizeByObject[targetIndex].second;
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		unsigned int newCandId = newByOldObjectId[simpit->first];
		if(simpit->second+std::min(targetSize,objects[newCandId].size())*maxValByNewObjectId[newCandId]*targetMaxVal>std::min(inTailSizeByObject[simpit->first].second,targetMinSim)){
			simpit->second  += dot(objects[newCandId],targetObject);
		}
	}
}//end of completeSimliarities

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<unsigned int>& newByOldObjectId){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		simpit->second  += dot(objects[newByOldObjectId[simpit->first]],objects[newByOldObjectId[targetIndex]]);
	}
}//end of completeSimliarities

void Computer::processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, double norm){
	//object = [key,val,key,val, ... , key,val]
	std::vector<std::string>::iterator featit;
	bool isKey = true;
	unsigned int key = 0;
	double val = 0; 
	for(featit=object.begin(); featit!=object.end(); ++featit){
		if(isKey){
			key = atoi(featit->c_str()); 
			isKey = false;
		}
		else{
			val = atof(featit->c_str())/norm; 
			targetFeatures.push_back(std::make_pair(key,val));
			// std::cout << "(" << key << "," << val << "),";
			isKey = true;
		}
	} //end of for
	// std::cout << std::endl << std::flush;
}//end of processObject

void Computer::fillObjectFloorPairs(std::list<std::pair<unsigned int,double>>& objectFloorPairs, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<bool>& followerByOldId){
	std::vector<std::pair<unsigned int,double>>::iterator tailit;
	unsigned int objectIndex = 0;
	for(tailit=inTailSizeByObject.begin(); tailit!=inTailSizeByObject.end(); ++tailit){
		if(followerByOldId[objectIndex]) objectFloorPairs.push_back(std::make_pair(objectIndex,tailit->second));
		// std::cout<< "floor," <<tailit->second<<std::endl<<std::flush;
		++objectIndex;
	}
}//end of fillObjectFloorPairs

void Computer::mapFeatures(std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId){
	featureCountPairs.sort(compare_pairs_second_int_bib);
	std::list<std::pair<unsigned int, unsigned int>>::iterator pit;
	unsigned int newFeatureId = 0;
	for(pit=featureCountPairs.begin(); pit!=featureCountPairs.end(); ++pit){
		oldByNewFeatureId[newFeatureId] = pit->first;
		newByOldFeatureId[pit->first] = newFeatureId;
		++newFeatureId;
	}

}//end of mapFeatures

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double& maxIndexSim){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex){
			simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
			maxIndexSim = std::max(maxIndexSim,similarities[*linkit]);
		}
	}
}//end of computeSimilarities

std::tuple<unsigned int,unsigned int,std::vector<double>,std::list<std::pair<unsigned int,unsigned int>>> Computer::getGlobalStats(std::string inputFileName){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	std::unordered_set<unsigned int> features;
	std::vector<double> norms;
	std::unordered_map<unsigned int,unsigned int> countByFeatureMap;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				unsigned int featureId = atoi(featit->c_str());
				features.insert(featureId);
				countByFeatureMap[featureId] += 1;
				isKey = false;
			} else {
				sqnorm += pow(atof(featit->c_str()),2.0);
				isKey = true;
			}
		} //end of for
		norms.push_back(sqrt(sqnorm));
		++objectIndex;
	}//end of while
	inputFile.close();
	//transform map to vector
	std::list<std::pair<unsigned int,unsigned int>> featureCountPairs;
	std::unordered_map<unsigned int,unsigned int>::iterator pit;
	for(pit=countByFeatureMap.begin();pit!=countByFeatureMap.end();++pit) featureCountPairs.push_back(*pit);
	return std::make_tuple(objectIndex,features.size(),norms,featureCountPairs);
}//end of count

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		// std::cout << "feature " << featit->first << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		// std::cout << "size of object list = " << objectsByFeature[featit->first].size() << std::endl << std::flush;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			// std::cout << "object " << candit->first << std::endl << std::flush;
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

double Computer::half_dense_dot(std::vector<std::pair<unsigned int,double>>& v1, std::vector<double>& v2){
	//assumes v1 and v2 are sorted from low to high key (unsigned int).
	double dot=0;
	std::vector<std::pair<unsigned int,double>>::iterator vit1;
	for(vit1=v1.begin(); vit1!=v1.end(); ++vit1) dot += vit1->second*v2[vit1->first];
	return dot;
}

double Computer::dot(std::vector<std::pair<unsigned int,double>>& v1, std::vector<std::pair<unsigned int,double>>& v2){
	//assumes v1 and v2 are sorted from low to high key (unsigned int).
	double dot=0;
	std::vector<std::pair<unsigned int,double>>::iterator vit1,vit2;
	vit1 = v1.begin();
	vit2 = v2.begin();

	// while((vit1!=v1.end()) && (vit2!=v2.end())){
	// 	if(vit1->first < vit2->first){
	// 		++vit1;
	// 	} else if(vit1->first > vit2->first){
	// 		++vit2;
	// 	} else {
	// 		dot += vit1->second*vit2->second;
	// 		++vit1;
	// 		++vit2;
	// 	}
	// }

	bool cont = ((vit1!=v1.end()) && (vit2!=v2.end()));
	while(cont){
		if(vit1->first < vit2->first){
			++vit1;
			if(vit1==v1.end()) cont = false;
		} else if(vit1->first > vit2->first){
			++vit2;
			if(vit2==v2.end()) cont = false;
		} else {
			dot += vit1->second*vit2->second;
			++vit1;
			++vit2;
			if( (vit1==v1.end()) || (vit2==v2.end()) ) cont = false;
		}
	}

	return dot;
}

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		simpit->second  += dot(objects[simpit->first],objects[targetIndex]);
	}
}//end of completeSimliarities

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, double& maxVal){
	// std::cout << "in add to index" << std::endl << std::flush;
	double b = 0;
	unsigned int count = 1;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = targetFeatures.begin();
	std::vector<double> vals;
	while((stayit!=targetFeatures.end()) && (b<minSim)){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		vals.push_back(stayit->second);
		++stayit;
		++count;
	}
	vals.pop_back();
	maxVal = *std::max_element(vals.begin(),vals.end());
	if(b>=minSim){
		count -= 2;
		unsigned int size = targetFeatures.size();
		while(size>count){
			objectsByFeature[targetFeatures.back().first].push_back(std::make_pair(targetIndex,targetFeatures.back().second));
			targetFeatures.pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId){
	// std::cout << "in add to index" << std::endl << std::flush;
	double b = 0;
	unsigned int count = 1;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = targetFeatures.begin();
	while((stayit!=targetFeatures.end()) && (b<minSim-kFudgeFactor)){
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		++stayit;
		++count;
	}
	if(b>=minSim-kFudgeFactor){
		count -= 2;
		unsigned int size = targetFeatures.size();
		while(size>count){
			objectsByFeature[targetFeatures.back().first].push_back(std::make_pair(targetIndex,targetFeatures.back().second));
			targetFeatures.pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::loadObjects(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewFeatureId, std::vector<double>& norms){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				double val = atof(featit->c_str())/norms[oldObjectId];
				objects[newByOldObjectId[oldObjectId]].push_back(std::make_pair(newByOldFeatureId[key],val));
				maxValByNewFeatureId[newByOldFeatureId[key]] = std::max(maxValByNewFeatureId[newByOldFeatureId[key]],val);
				isKey = true;
			}
		} //end of for
		sort(objects[newByOldObjectId[oldObjectId]].begin(),objects[newByOldObjectId[oldObjectId]].end(),compare_pairs_first_sib);
		++oldObjectId;
	}
	inputFile.close();
}//end of loadObjects

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& norms, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		// std::cout << "feature " << featit->first << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		// std::cout << "size of object list = " << objectsByFeature[featit->first].size() << std::endl << std::flush;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			// std::cout << "object " << candit->first << std::endl << std::flush;
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second)/(norms[candit->first]*norms[targetIndex]);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex){
			simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
		}
	}
}//end of computeSimilarities

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::list<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::list<std::pair<unsigned int,double>>>& objectsByFeature){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::list<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		std::list<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::writeTopks(std::string controlFileName, std::vector<topkType>& topkByObject, std::vector<unsigned int>& oldByNewObjectId){
	// current method takes n log k i.s.o n time complexity;
	std::ofstream controlFile(controlFileName);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByObject.begin(); topkit!=topkByObject.end(); ++topkit){
		controlFile << oldByNewObjectId[objectIndex];
		while(topkit->size()>0){
			controlFile << "," << oldByNewObjectId[topkit->top().first];
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks

void Computer::addToIndex(unsigned int targetIndex, std::list<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::list<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId){
	double b = 0;
	std::list<std::pair<unsigned int, double>>::iterator featit;
	featit = targetFeatures.begin();
	while((featit!=targetFeatures.end()) && (b<minSim)){
		b += featit->second*maxValByNewFeatureId[featit->first];
		++featit;
	}
	if(b>=minSim){
		--featit;
		while(featit!=targetFeatures.end()){
			objectsByFeature[featit->first].push_back(std::make_pair(targetIndex,featit->second));
			featit = targetFeatures.erase(featit); //could maybe be slightly optimized if the method is rewritten to pop_back instead of erase.

		}
	}
}//end of completeSimilarities

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		simpit->second  += dot(objects[simpit->first],objects[targetIndex]);
	}
}//end of completeSimliarities

double Computer::dot(std::list<std::pair<unsigned int,double>>& v1, std::list<std::pair<unsigned int,double>>& v2){
	//assumes v1 and v2 are sorted from low to high key (unsigned int).
	double dot=0;
	std::list<std::pair<unsigned int,double>>::iterator vit1,vit2;
	vit1 = v1.begin();
	vit2 = v2.begin();
	while((vit1!=v1.end()) && (vit2!=v2.end())){
		if(vit1->first < vit2->first){
			++vit1;
		} else if(vit1->first > vit2->first){
			++vit2;
		} else {
			dot += vit1->second*vit2->second;
			++vit1;
			++vit2;
		}
	}
	return dot;
}

void Computer::loadObjects(std::string inputFileName, std::vector<std::list<std::pair<unsigned int,double>>>& objects, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& newByOldObjectId, std::vector<double>& maxValByNewFeatureId, std::vector<double>& norms){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				double val = atof(featit->c_str())/norms[oldObjectId];
				objects[newByOldObjectId[oldObjectId]].push_back(std::make_pair(newByOldFeatureId[key],val));
				maxValByNewFeatureId[newByOldFeatureId[key]] = std::max(maxValByNewFeatureId[newByOldFeatureId[key]],val);
				isKey = true;
			}
		} //end of for
		objects[newByOldObjectId[oldObjectId]].sort(compare_pairs_first_sib);
		++oldObjectId;
	}
	inputFile.close();
}//end of loadObjects

void Computer::mapFeatures(std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<unsigned int>& countByNewFeatureId, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId){
	featureCountPairs.sort(compare_pairs_second_int_bib);
	std::list<std::pair<unsigned int, unsigned int>>::iterator pit;
	unsigned int newFeatureId = 0;
	for(pit=featureCountPairs.begin(); pit!=featureCountPairs.end(); ++pit){
		oldByNewFeatureId[newFeatureId] = pit->first;
		newByOldFeatureId[pit->first] = newFeatureId;
		countByNewFeatureId[newFeatureId] = pit->second;
		++newFeatureId;
	}

}//end of mapFeatures

void Computer::mapObjects(std::list<std::pair<unsigned int, double>>& objectFloorPairs, std::vector<double>& floorByNewObjectId, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId){
	objectFloorPairs.sort(compare_pairs_second_sib);
	std::list<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newObjectId = 0;
	for(pit=objectFloorPairs.begin(); pit!=objectFloorPairs.end(); ++pit){
		oldByNewObjectId[newObjectId] = pit->first;
		newByOldObjectId[pit->first] = newObjectId;
		floorByNewObjectId[newObjectId] = pit->second;
		++newObjectId;
	}

}//end of mapObjects

void Computer::extractStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::list<std::pair<unsigned int,double>>& objectFloorPairs, std::list<std::pair<unsigned int, unsigned int>>& featureCountPairs, std::vector<double>& norms){
	std::cout << "start to extract stats" << std::endl << std::flush;
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>> objectsByFeature;
	std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>> featuresByObject;
	noObjects = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				double val = atof(featit->c_str());
				sqnorm += pow(val,2.0);
				objectsByFeature[key].push_back(std::make_pair(noObjects,val));
				// std::cout << "put object " << noObjects << "in list of feature " << key << std::endl << std::flush;
				featuresByObject[noObjects].push_back(std::make_pair(key,val));
				isKey = true;
			}
		} //end of for
		norms.push_back(sqrt(sqnorm));
		++noObjects;
	}//end of while
	inputFile.close();

	//set output noFeatures
	noFeatures = objectsByFeature.size();
	// std::cout << "noFeatures = " << noFeatures << std::endl << std::flush;


	//process objectsByFeature
	std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>>::iterator featit;
	for(featit=objectsByFeature.begin(); featit!=objectsByFeature.end(); ++featit){
		featureCountPairs.push_back(std::make_pair(featit->first,featit->second.size()));
		//replace full list by top-k*m with k*m>=2
		unsigned int t = std::max(k*m,(unsigned int)2);
		// std::cout << "oldest size = " << featit->second.size() << std::endl << std::flush;
		std::nth_element(featit->second.begin(),featit->second.begin()+(t-1),featit->second.end(),compare_pairs_second_bib);
		// std::cout << "old size = " << featit->second.size() << std::endl << std::flush;
		featit->second.resize(t); //might be not fully correct due to ties, but this is just for getting a lower bound, no correctness required
		// std::cout << "new size = " << featit->second.size() << std::endl << std::flush;
	}


	//compute floors
	std::unordered_map<unsigned int,std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int targetIndex = 0;
	for(objit=featuresByObject.begin(); objit!=featuresByObject.end(); ++objit){
		std::vector<std::pair<unsigned int,double>> simPairs;
		// std::cout << "target index = " << targetIndex << std::endl << std::flush;
		computeSimilarities(noObjects,simPairs,norms,targetIndex,objit->second,objectsByFeature); 
		std::nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		// std::cout << "size of simpairs = " << simPairs.size() << std::endl << std::flush;
		objectFloorPairs.push_back(std::make_pair(targetIndex,simPairs[k-1].second));
		// std::cout << "floor, " << simPairs[k-1].second << std::endl << std::flush;
		++targetIndex;
	};
}//end of extractStats

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	
	std::vector<std::pair<unsigned int,double>>::reverse_iterator featit;
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		updateOffset(offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].end(),sizeByNewObjectId,minSize);
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first]; candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]<=kFudgeFactor) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::pair<unsigned int,double>>& object, unsigned int& objectOffset, unsigned int& increment, double& b, std::vector<double>& maxWByNewFeatureId, double& maxObjectW){
	std::vector<std::pair<unsigned int,double>>::reverse_iterator tupit = object.rbegin()+objectOffset;
	while((tupit!=object.rend()) && !(threshold-b>kFudgeFactor)){
		objectsByFeature[tupit->first].push_back(std::make_pair(targetIndex,tupit->second));
		b -= tupit->second*std::min(maxWByNewFeatureId[tupit->first],maxObjectW);
		++objectOffset;
		++increment;
		++tupit;
	}
}//end of addToIndex

void Computer::readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<size_t>& sizeByNewObjectId, std::vector<double>& bByObject, double t, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		unsigned int newObjectId = newByOldObjectId[oldObjectId];
		std::vector<std::pair<unsigned int,double>>* op = &objects[newObjectId];
		std::vector<std::string>::iterator featit;
		size_t size = 0;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str())/normByOldObjectId[oldObjectId];
				op->push_back(std::make_pair(newByOldFeatureId[key],val));
				++size;
				isKey = true;
			}
		}
		sizeByNewObjectId[newObjectId] = size;
		std::sort(op->begin(),op->end(),compare_pairs_first_sib);

		//compute b
		double maxW = maxWByNewObjectId[newObjectId];
		std::vector<std::pair<unsigned int,double>>::iterator pit = op->begin();
		while(pit!=op->end()){
			bByObject[newObjectId] += pit->second*std::min(maxWByNewFeatureId[pit->first],maxW);
			++pit;
		}


		++oldObjectId;
	}
	inputFile.close();
}//end of readObjects

void Computer::idFinishedObjects(std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, double threshold, std::vector<bool>& unfinishedByNewObjectId){
	std::vector<std::pair<unsigned int,double>>::iterator tailit;
	unsigned int targetIndex = 0;
	for(tailit=inTailSizeByObject.begin(); tailit!=inTailSizeByObject.end(); ++tailit){
		if(tailit->second>=threshold-kFudgeFactor) unfinishedByNewObjectId[targetIndex]=false;
		++targetIndex;
	}
}//end of idFinishedObjects

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects){
	std::vector<std::tuple<unsigned int,double,double>>* tp = &objects[targetIndex];
	while(!tp->empty()){
		objectsByFeature[std::get<0>(tp->back())].push_back(std::make_pair(targetIndex,std::get<1>(tp->back())));
		tp->pop_back();
	}
}//end of addToIndex

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects){
	std::vector<std::tuple<unsigned int,double,double>>* tp = &objects[targetIndex];
	while( (!tp->empty()) && (std::get<2>(tp->back())>=threshold-kFudgeFactor) ){
		objectsByFeature[std::get<0>(tp->back())].push_back(std::make_pair(targetIndex,std::get<1>(tp->back())));
		tp->pop_back(); 
	}
}//end of addToIndex

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double threshold, std::vector<std::tuple<unsigned int,double,double>>& object, unsigned int& objectOffset, unsigned int& increment){
	std::vector<std::tuple<unsigned int,double,double>>::reverse_iterator tupit = object.rbegin()+objectOffset;
	// if(targetIndex==3732) std::cout << "in add to index. b to start with = " << std::get<2>(*tupit) << std::endl << std::flush;
	while((tupit!=object.rend()) && (std::get<2>(*tupit)>=threshold-kFudgeFactor)){
		// if(targetIndex==3732) std::cout << "index feature" << std::get<0>(*tupit) << " of new object id 3732" << std::endl << std::flush;
		objectsByFeature[std::get<0>(*tupit)].push_back(std::make_pair(targetIndex,std::get<1>(*tupit)));
		++objectOffset;
		++increment;
		++tupit;
	}
}//end of addToIndex

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::tuple<unsigned int,double,double>>& object, unsigned int& objectOffset){
	std::vector<std::tuple<unsigned int,double,double>>::reverse_iterator tupit = object.rbegin()+objectOffset;
	while(tupit!=object.rend()){
		// if(targetIndex==3732) std::cout << "index feature" << std::get<0>(*tupit) << " of new object id 3732"<< std::endl << std::flush;
		objectsByFeature[std::get<0>(*tupit)].push_back(std::make_pair(targetIndex,std::get<1>(*tupit)));
		++objectOffset;
		++tupit;
	}
}//end of addToIndex

double Computer::dot(std::vector<std::tuple<unsigned int,double,double>>& v1, std::vector<std::tuple<unsigned int,double,double>>& v2){
	//assumes v1 and v2 are sorted from low to high key (unsigned int).
	double dot=0;
	std::vector<std::tuple<unsigned int,double,double>>::iterator vit1,vit2;
	vit1 = v1.begin();
	vit2 = v2.begin();
	while((vit1!=v1.end()) && (vit2!=v2.end())){
		if(std::get<0>(*vit1) < std::get<0>(*vit2)){
			++vit1;
		} else if(std::get<0>(*vit1) > std::get<0>(*vit2)){
			++vit2;
		} else {
			dot += std::get<1>(*vit1)*std::get<1>(*vit2);
			++vit1;
			++vit2;
		}
	}
	return dot;
}

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold){
	std::vector<std::tuple<unsigned int,double,double>>* tfp = &objects[targetIndex];
	size_t targetSize = tfp->size();
	double targetMaxW = maxWByNewObjectId[targetIndex];

	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		if(simpit->second+std::min(targetSize,objects[simpit->first].size())*maxWByNewObjectId[simpit->first]*targetMaxW >= threshold-kFudgeFactor){
			simpit->second  += dot(objects[simpit->first],*tfp);
		}
	}
}//end of completeSimilarities

void Computer::emptyIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& index){
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator indit;
	for(indit=index.begin();indit!=index.end();++indit){
		indit->resize(0); //no deallocation. alternative with deallocation is std::vector<std::pair<unsigned int,double>>().swap(*indit);. Do not use clear() as it is not known whether there will be deallocation or not.
	}
}// end of emtpyIndex

void Computer::transferIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& index, std::vector<std::vector<std::pair<unsigned int,double>>>& objects){
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator indit;
	unsigned int featureIndex = 0;
	for(indit=index.begin();indit!=index.end();++indit){
		while(!indit->empty()){
			objects[indit->back().first].push_back(std::make_pair(featureIndex,indit->back().second));
			indit->pop_back();
		}
		++featureIndex;
	}
}// end of transferIndex

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::tuple<unsigned int, double,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	
	std::vector<std::tuple<unsigned int,double,double>>::reverse_iterator featit;
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		updateOffset(offsetByNewFeatureId[std::get<0>(*featit)],objectsByFeature[std::get<0>(*featit)].begin()+offsetByNewFeatureId[std::get<0>(*featit)],objectsByFeature[std::get<0>(*featit)].end(),sizeByNewObjectId,minSize);
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[std::get<0>(*featit)].begin()+offsetByNewFeatureId[std::get<0>(*featit)]; candit!=objectsByFeature[std::get<0>(*featit)].end(); ++candit){
			if(similarities[candit->first]<=kFudgeFactor) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (std::get<1>(*featit))*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objects, std::vector<size_t>& sizeByNewObjectId, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId){
	double t = *std::max_element(t_vec.begin(),t_vec.end());
	std::cout << "max threshold = " << t << std::endl << std:: flush;

	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		unsigned int newObjectId = newByOldObjectId[oldObjectId];
		std::vector<std::tuple<unsigned int,double,double>>* op = &objects[newObjectId];
		std::vector<std::string>::iterator featit;
		size_t size = 0;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str())/normByOldObjectId[oldObjectId];
				op->push_back(std::make_tuple(newByOldFeatureId[key],val,maxSim));
				++size;
				isKey = true;
			}
		}
		sizeByNewObjectId[newObjectId] = size;
		std::sort(op->begin(),op->end(),compare_triplets_first_sgf);

		//compute b's
		double b = 0;
		std::vector<std::tuple<unsigned int,double,double>>::iterator tripit = op->begin();
		while((b<t-kFudgeFactor) && (tripit!=op->end())){
			b += std::get<1>(*tripit)*std::min(maxWByNewFeatureId[std::get<0>(*tripit)],maxWByNewObjectId[newObjectId]);
			// if(newObjectId==3732) std::cout << "for 3732, b = " << b << " = " << std::get<1>(*tripit) << " * min( " << maxWByNewFeatureId[std::get<0>(*tripit)] << "," << maxWByNewObjectId[newObjectId] << ")" << std::endl << std::flush; 
			std::get<2>(*tripit) = b;
			++tripit;
		}

		++oldObjectId;
	}
	inputFile.close();
}//end of readObjects

void Computer::allNN(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm allNN" << std::endl << std::flush;

	//initalize timer
	clock_t start = clock();
	// std::cout.precision(15);

	//first pass over data
	std::cout << "before extractGlobalStats" << std::endl << std::flush;
	unsigned int noObjects = 0;
	unsigned int noFeatures = 0;
	std::vector<double> normByOldObjectId;
	std::vector<std::pair<unsigned int,double>> featureValPairs;
	std::vector<std::pair<unsigned int,double>> objectValPairs;
	std::unordered_map<unsigned int,double> maxWByOldFeatureId;
	std::unordered_map<unsigned int,double> maxWByOldObjectId;
	std::unordered_map<unsigned int,double> sumByOldObjectId;
	extractGlobalStats(inputFileName, noObjects,noFeatures,normByOldObjectId,featureValPairs,objectValPairs,maxWByOldFeatureId,maxWByOldObjectId,sumByOldObjectId);

	std::cout << "#features: " << noFeatures << std::endl << std::flush;
	std::cout << "#objects: " << noObjects << std::endl << std::flush;

	std::cout << "before mapping features" << std::endl << std::flush;
	std::vector<unsigned int> newByOldFeatureId(noFeatures);
	std::vector<unsigned int> oldByNewFeatureId(noFeatures);
	std::vector<double> maxWByNewFeatureId(noFeatures);
	mapFeatures_bgf(featureValPairs,newByOldFeatureId,oldByNewFeatureId,maxWByOldFeatureId,maxWByNewFeatureId);

	std::cout << "before mapping objects" << std::endl << std::flush;
	std::vector<unsigned int> newByOldObjectId(noObjects);
	std::vector<unsigned int> oldByNewObjectId(noObjects);
	std::vector<double> maxWByNewObjectId(noObjects);
	std::vector<double> sumByNewObjectId(noObjects);
	mapObjects_bgf(objectValPairs,newByOldObjectId,oldByNewObjectId,maxWByOldObjectId,maxWByNewObjectId,sumByOldObjectId,sumByNewObjectId);

	//load objects
	std::vector<std::vector<std::pair<unsigned int,double>>> objects(noObjects);
	std::vector<size_t> sizeByNewObjectId(noObjects,0);
	readObjects(inputFileName,newByOldObjectId,newByOldFeatureId,normByOldObjectId,objects,sizeByNewObjectId);

	//initialize vectors of correct size
	std::vector<std::vector<std::tuple<unsigned int,double,double>>> objectsByFeature (noFeatures);
	std::vector<topkType> topkByNewObjectId;
	std::vector<std::pair<unsigned int,double>> inTailSizeByObject(noObjects,std::make_pair(0,maxSim));
	std::vector<unsigned int> offsetByNewFeatureId(noFeatures,0);
	std::vector<double> pscore(noObjects,0);
	std::vector<double> prefixSqNorm(noObjects,0);

	//estimate threshold
	clock_t threshold_beacon = clock();
	double estimated_threshold;
	estimate_threshold(objects,noObjects,noFeatures,estimated_threshold);
	writeTiming(timingFileName, (clock()-threshold_beacon)/double(CLOCKS_PER_SEC), "threshold_step_time[s]");
	std::cout << "threshold_step_time[s] is " << (clock()-threshold_beacon)/double(CLOCKS_PER_SEC) << std:: endl << std::flush;

	//use estimated threshold
	threshold = estimated_threshold;
	std::cout << "threshold used: " << threshold << std::endl << std::flush;

	//timing
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "preproc_time[s]");
	clock_t preproc_beacon = clock();

	//for all objects
	std::cout << "before loop over inputFile" << std::endl << std::flush;
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int targetIndex = 0; 
	for(objit=objects.begin(); objit!=objects.end(); ++objit){

		double minSize = threshold/maxWByNewObjectId[targetIndex];

		std::vector<std::pair<unsigned int,double>> simPairs;
		computeSimilarities_allnn(noObjects,simPairs,targetIndex,*objit,objectsByFeature,maxWByNewFeatureId,threshold,sizeByNewObjectId,offsetByNewFeatureId,minSize);

		completeSimilarities_allnn(simPairs,objects,targetIndex,maxWByNewObjectId,threshold,pscore,prefixSqNorm,noFeatures,sumByNewObjectId);// allNN version when allNN pruning is introduced

		//initalize top-k of object
		initTopk(simPairs,topkByNewObjectId,inTailSizeByObject[targetIndex],targetIndex);


		//update top-k of previous objects
		updateLinkedTopks(targetIndex,simPairs,topkByNewObjectId,inTailSizeByObject);


		//add object to inverted indices
		addToIndex_allnn(targetIndex,objectsByFeature,threshold,maxWByNewFeatureId,maxWByNewObjectId,objects,pscore,prefixSqNorm,sumByNewObjectId[targetIndex]);

		++targetIndex;
	}
	writeTiming(timingFileName, (clock()-preproc_beacon)/double(CLOCKS_PER_SEC), "first_step_time[s]");
	clock_t first_step_beacon = clock();

	restoreObjects_allnn(objects,objectsByFeature);
	std::vector<std::vector<std::pair<unsigned int,double>>> objectPairsByFeature (noFeatures);

	std::vector<bool> finishedByNewObjectId(noObjects,true);
	std::vector<unsigned int> unfinishedObjects;
	idUnfinishedObjects(inTailSizeByObject,unfinishedObjects,finishedByNewObjectId,threshold);

	std::cout << "#unfinishedObjects: " << unfinishedObjects.size() << std::endl<<std::flush;
	writeTiming(timingFileName, (clock()-first_step_beacon)/double(CLOCKS_PER_SEC), "restore_time[s]");
	clock_t restore_beacon = clock();

	dynamicIndex(objects,objectPairsByFeature,inTailSizeByObject,unfinishedObjects,topkByNewObjectId,noObjects,noFeatures,maxWByNewFeatureId);
	
	writeTiming(timingFileName, (clock()-restore_beacon)/double(CLOCKS_PER_SEC), "dynamic_time[s]");
	clock_t dyn_beacon = clock();

	frozenIndex(objects,objectPairsByFeature,inTailSizeByObject,finishedByNewObjectId,topkByNewObjectId,noObjects,noFeatures);
	
	writeTiming(timingFileName, (clock()-dyn_beacon)/double(CLOCKS_PER_SEC), "frozen_time[s]");

	writeTopks_elimDups(controlFileName,topkByNewObjectId,oldByNewObjectId);

	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
	std::cout << "after writing" << std::endl << std::flush;
}//end of allNN

void Computer::estimate_threshold(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int noObjects, unsigned int noFeatures, double& estimated_threshold){
	//build (partial?) index
	std::vector<std::vector<std::pair<unsigned int,double>>> objectsByFeature (noFeatures);
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	unsigned int objectId = 0;
	for(objit=objects.begin(); objit!=objects.end(); ++objit){
		std::vector<std::pair<unsigned int,double>>::iterator pairit;
		for(pairit=objit->begin(); pairit!=objit->end(); ++pairit){
			objectsByFeature[pairit->first].push_back(std::make_pair(objectId,pairit->second));
		}
		++objectId;
	}
	//compute sim of k-most similar for x objects
	unsigned int minTestObjects = 500;
	unsigned int noTestObjects = std::min(noObjects/10000, minTestObjects);
	std::vector<double> kmostSimilars;
	unsigned int targetIndex;
	srand(0);
	while(noTestObjects>0){
		targetIndex = rand() % noObjects;
		std::vector<double> similarities(noObjects,0);
		std::vector<std::pair<unsigned int,double>> simPairs;
		computeSimilarities(noObjects,simPairs,targetIndex,objects[targetIndex],objectsByFeature); 
		extractAtk(simPairs,kmostSimilars);
		--noTestObjects;
	}//end for over all targets
	//sort values
	std::sort(kmostSimilars.begin(),kmostSimilars.end());
	//compute correct index
	unsigned int index = kmostSimilars.size()/10;
	//get value at correct index, this is the estimated threshold
	estimated_threshold = kmostSimilars[index];
	std::cout << "estimated threshold is " << estimated_threshold << std::endl << std::flush;
}//end of estimate_threshold

void Computer::extractAtk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& kmostSimilars){
	//+(k-1) because of first index = 0.
	if(simPairs.size()>k){
		nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		kmostSimilars.push_back(simPairs[k-1].second);
	} else if(simPairs.size()>0) {
		double smallest = (std::min_element(simPairs.begin(),simPairs.end(),compare_pairs_second_sib))->second;
		kmostSimilars.push_back(smallest);
	} else {
		kmostSimilars.push_back(0);
	}
}//end of extractTopk

void Computer::mapObjects_bgf(std::vector<std::pair<unsigned int, double>>& objectValPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId,std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::vector<double>& maxWByNewObjectId, std::unordered_map<unsigned int,double>& sumByOldObjectId, std::vector<double>& sumByNewObjectId){
	std::sort(objectValPairs.begin(),objectValPairs.end(),compare_pairs_second_bib);
	std::vector<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newObjectId = 0;
	for(pit=objectValPairs.begin(); pit!=objectValPairs.end(); ++pit){
		oldByNewObjectId[newObjectId] = pit->first;
		newByOldObjectId[pit->first] = newObjectId;
		maxWByNewObjectId[newObjectId] = maxWByOldObjectId[pit->first];
		sumByNewObjectId[newObjectId] = sumByOldObjectId[pit->first];
		++newObjectId;
	}
}//end of mapFeatures

void Computer::extractGlobalStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::unordered_map<unsigned int,double>& sumByOldObjectId){

	std::unordered_map<unsigned int,unsigned int> countByOldFeatureId;

	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	noObjects = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		std::vector<std::pair<unsigned int,double>> temp;
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				++countByOldFeatureId[key];
				isKey = false;
			} else {
				val = atof(featit->c_str());
				sqnorm += pow(val,2.0);
				temp.push_back(std::make_pair(key,val));
				isKey = true;
			}
		}
		double norm = sqrt(sqnorm);
		normByOldObjectId.push_back(norm);
		std::vector<std::pair<unsigned int,double>>::iterator pit;
		double normalized = 0;
		for(pit=temp.begin(); pit!=temp.end(); ++pit){
			normalized = pit->second/norm;
			maxWByOldObjectId[noObjects] = std::max(normalized,maxWByOldObjectId[noObjects]);
			maxWByOldFeatureId[pit->first] = std::max(normalized,maxWByOldFeatureId[pit->first]);
			sumByOldObjectId[noObjects] += normalized;
		}
		++noObjects;
	}
	inputFile.close();
	noFeatures = countByOldFeatureId.size();
	std::unordered_map<unsigned int,unsigned int>::iterator upit;
	for(upit=countByOldFeatureId.begin(); upit!=countByOldFeatureId.end(); ++upit) featureValPairs.push_back(std::make_pair(upit->first,(double)upit->second));
	std::unordered_map<unsigned int,double>::iterator vpit;
	for(vpit=maxWByOldObjectId.begin(); vpit!=maxWByOldObjectId.end(); ++vpit) objectValPairs.push_back(*vpit);
}//end of extractGlobalStats

void Computer::completeSimilarities_allnn(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold, std::vector<double>& pscore, std::vector<double>& prefixSqNorm, unsigned int noFeatures, std::vector<double>& sumByNewObjectId){
	std::vector<std::pair<unsigned int,double>>* tfp = &objects[targetIndex];

	size_t targetSize = tfp->size();
	double targetMaxW = maxWByNewObjectId[targetIndex];

	std::vector<std::pair<unsigned int,double>> keep;
	keep.reserve(simPairs.size());
	while(!simPairs.empty()){
		if(simPairs.back().second+std::min(targetSize,objects[simPairs.back().first].size())*maxWByNewObjectId[simPairs.back().first]*targetMaxW >= threshold-kFudgeFactor){ //dp5

			simPairs.back().second  += dot(objects[simPairs.back().first],*tfp);
			keep.push_back(simPairs.back());
			simPairs.pop_back();

		} else {
			simPairs.pop_back();
		}
	}
	simPairs = keep;
}//end of completeSimilarities_allnn

void Computer::restoreObjects_allnn(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature){
	std::vector<std::vector<std::tuple<unsigned int,double,double>>>::iterator featit;
	unsigned int featureIndex = 0;
	for(featit=objectsByFeature.begin(); featit!=objectsByFeature.end(); ++featit){
		while(!featit->empty()){
			objects[std::get<0>(featit->back())].push_back(std::make_pair(featureIndex,std::get<1>(featit->back())));
			featit->pop_back();
		}
		++featureIndex;
	}
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	for(objit=objects.begin(); objit!=objects.end(); ++objit){
		std::sort(objit->begin(), objit->end(), compare_pairs_first_sib);
	}
}//end of restoreObjects

void Computer::addToIndex_allnn(unsigned int targetIndex, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, double minSim, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<double>& pscore, std::vector<double>& prefixSqNorm, double& targetSum){
	// std::cout << "in add to index" << std::endl << std::flush;
	std::vector<std::pair<unsigned int,double>>* tfp = &objects[targetIndex];
	double b1 = 0;
	double bt = 0;
	double b3 = 0;
	double newMax = 0;
	double newSum = 0;
	unsigned int remainingSize = 0;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = tfp->begin();

	bool cont = (stayit!=tfp->end());
	while(cont){
		b1 += stayit->second*std::min(maxWByNewFeatureId[stayit->first],maxWByNewObjectId[targetIndex]);
		bt += pow(stayit->second,2);
		b3 = sqrt(bt);
		if((stayit!=tfp->end()) && ((minSim-std::min(b1,b3))>kFudgeFactor)){
			newMax = std::max(newMax,stayit->second);
			newSum += stayit->second;
			++stayit;
			++remainingSize;
		} else {
			cont = false;
		}
	}


	if((minSim-std::min(b1,b3))<=kFudgeFactor){
		bt -= pow(stayit->second,2);
		b1 -= stayit->second*std::min(maxWByNewFeatureId[stayit->first],maxWByNewObjectId[targetIndex]);
		b3 = sqrt(bt);
		pscore[targetIndex] = std::min(b1,b3);
		prefixSqNorm[targetIndex] = bt;

		while(stayit!=tfp->end()){
			objectsByFeature[stayit->first].push_back(std::make_tuple(targetIndex,stayit->second,b3));
			bt += pow(stayit->second,2);
			b3 = sqrt(bt);
			++stayit;
		}

		unsigned int size = tfp->size();
		while(size>remainingSize){
			tfp->pop_back();
			--size;
		}
	}

	//change max(x) with max(x')
	maxWByNewObjectId[targetIndex] = newMax;
	targetSum = newSum;

}//end of addToIndex

void Computer::updateOffset_allnn(unsigned int& offset, std::vector<std::tuple<unsigned int,double,double>>::iterator currentStart, std::vector<std::tuple<unsigned int,double,double>>::iterator end, std::vector<size_t>& sizeByNewObjectId, double minSize){
	bool cont = true;
	while(cont){
		if(currentStart==end) cont = false;
		else if(sizeByNewObjectId[std::get<0>(*currentStart)]<(minSize-kFudgeFactor)) ++currentStart,++offset;
		else cont = false;
	}
}//end of updateOffset_allnn

void Computer::computeSimilarities_allnn(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::tuple<unsigned int,double,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize){
	//iterator in common
	std::vector<std::pair<unsigned int,double>>::reverse_iterator featit;

	//for early termination

	//rs1
	// double remscore = 0;
	// for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
	// 	remscore += featit->second*maxWByNewFeatureId[featit->first];
	// }

	//rs4
	double remscore = 1;
	double prefixNorm = 1;
	double remsquare = 1;

	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){		
		remsquare -= pow(featit->second,2);
		prefixNorm = sqrt(remsquare);
		updateOffset_allnn(offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].end(),sizeByNewObjectId,minSize);
		std::vector<std::tuple<unsigned int,double,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first]; candit!=objectsByFeature[featit->first].end(); ++candit){
		// for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[std::get<0>(*candit)]>kFudgeFactor){
				similarities[std::get<0>(*candit)] += (featit->second)*(std::get<1>(*candit));
				if(threshold - similarities[std::get<0>(*candit)] - (prefixNorm*std::get<2>(*candit)) > kFudgeFactor){
						similarities[std::get<0>(*candit)] = -2;
				}
			} else {
				if ( ((threshold - remscore) <= kFudgeFactor) && (similarities[std::get<0>(*candit)]>-2) ){
					similarities[std::get<0>(*candit)] += (featit->second)*(std::get<1>(*candit));
					if(threshold - similarities[std::get<0>(*candit)] - (prefixNorm*std::get<2>(*candit)) > kFudgeFactor){
						similarities[std::get<0>(*candit)] = -2;
					} else {
						linkedObjects.push_back(std::get<0>(*candit));
					}
				}
				
			}
		}
		// remscore -= featit->second*maxWByNewFeatureId[featit->first];
		remscore = prefixNorm;

	}

	//transform sparse representation for quick looping
	simPairs.resize(linkedObjects.size()); //no targetIndex because dynamic
	for(unsigned int i = 0; i<simPairs.size(); ++i){
		if(similarities[linkedObjects[i]]>-2) simPairs[i] = std::make_pair(linkedObjects[i],similarities[linkedObjects[i]]);
	}
}//end of computeSimilarities_allnn

void Computer::updateOffset(unsigned int& offset, std::vector<std::pair<unsigned int,double>>::iterator currentStart, std::vector<std::pair<unsigned int,double>>::iterator end, std::vector<size_t>& sizeByNewObjectId, double minSize){
	bool cont = true;
	while(cont){
		if(currentStart==end) cont = false;
		else if(sizeByNewObjectId[currentStart->first]<(minSize-kFudgeFactor)) ++currentStart,++offset;
		else cont = false;
	}
}//end of updateOffset

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold, std::vector<size_t>& sizeByNewObjectId, std::vector<unsigned int>& offsetByNewFeatureId, double minSize){
	//commonly used iterator
	std::vector<std::pair<unsigned int,double>>::reverse_iterator featit;

	//for early termination
	double remscore = 0;
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		remscore += featit->second*maxWByNewFeatureId[featit->first];
	}

	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;

	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		updateOffset(offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first],objectsByFeature[featit->first].end(),sizeByNewObjectId,minSize);
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin()+offsetByNewFeatureId[featit->first]; candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]<=kFudgeFactor){ 
				if (remscore >= threshold-kFudgeFactor){
					linkedObjects.push_back(candit->first);
					similarities[candit->first] += (featit->second)*(candit->second);
				}
			} else {
				similarities[candit->first] += (featit->second)*(candit->second);
			}
		}
		remscore -= featit->second*maxWByNewFeatureId[featit->first];
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& maxWByNewFeatureId, double threshold){
	//commonly used iterator
	std::vector<std::pair<unsigned int,double>>::reverse_iterator featit;

	//for early termination
	double remscore = 0;
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		remscore += featit->second*maxWByNewFeatureId[featit->first];
	}

	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]<=kFudgeFactor){ 
				if (remscore>=threshold-kFudgeFactor){
					linkedObjects.push_back(candit->first);
					similarities[candit->first] += (featit->second)*(candit->second);
				}
			} else {
				similarities[candit->first] += (featit->second)*(candit->second);
			}
		}
		remscore -= featit->second*maxWByNewFeatureId[featit->first];
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxWByNewFeatureId, std::vector<double>& maxWByNewObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects){
	// std::cout << "in add to index" << std::endl << std::flush;
	std::vector<std::pair<unsigned int,double>>* tfp = &objects[targetIndex];
	double b = 0;
	unsigned int remainingSize = 0;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = tfp->begin();
	do{
		b += stayit->second*std::min(maxWByNewFeatureId[stayit->first],maxWByNewObjectId[targetIndex]);
		++stayit;
		++remainingSize;
	}while((stayit!=tfp->end()) && (minSim-b>kFudgeFactor));

	if(minSim-b<=kFudgeFactor){
		remainingSize -= 1;
		unsigned int size = tfp->size();
		while(size>remainingSize){
			objectsByFeature[tfp->back().first].push_back(std::make_pair(targetIndex,tfp->back().second));
			tfp->pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, unsigned int targetIndex, std::vector<double>& maxWByNewObjectId, double threshold){
	std::vector<std::pair<unsigned int,double>>* tfp = &objects[targetIndex];
	size_t targetSize = tfp->size();
	double targetMaxW = maxWByNewObjectId[targetIndex];

	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		if(simpit->second+std::min(targetSize,objects[simpit->first].size())*maxWByNewObjectId[simpit->first]*targetMaxW >= threshold-kFudgeFactor){
			simpit->second  += dot(objects[simpit->first],*tfp);
		}
	}
}//end of completeSimilarities

void Computer::frozenIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<bool>& finishedByNewObjectId, std::vector<topkType>& topkByNewObjectId, unsigned int noObjects, unsigned int noFeatures){
	std::vector<bool>::iterator objit;
	unsigned int targetIndex = 0;
	for(objit=finishedByNewObjectId.begin();objit!=finishedByNewObjectId.end();++objit){
		if(*objit){
			std::vector<std::pair<unsigned int,double>> simPairs;
			computeSimilarities(noObjects,simPairs,targetIndex,objects[targetIndex],objectsByFeature);
			updateLinkedTopks(targetIndex,simPairs,topkByNewObjectId,inTailSizeByObject);
		}
		++targetIndex;
	}
	// std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	// unsigned int targetIndex = 0;
	// for(objit=objects.begin(); objit!=objects.end(); ++objit){
	// 	if(finishedByNewObjectId[targetIndex]){
	// 		std::vector<std::pair<unsigned int,double>> simPairs;
	// 		computeSimilarities(noObjects,simPairs,targetIndex,*objit,objectsByFeature);
	// 		updateLinkedTopks(targetIndex,simPairs,topkByNewObjectId,inTailSizeByObject);
	// 	}
	// 	++targetIndex;
	// }
}//end of frozenIndex

void Computer::dynamicIndex(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<unsigned int>& unfinishedObjects, std::vector<topkType>& topkByNewObjectId, unsigned int noObjects, unsigned int noFeatures, std::vector<double>& maxWByNewFeatureId){
	std::vector<unsigned int>::iterator objit;
	for(objit=unfinishedObjects.begin();objit!=unfinishedObjects.end();++objit){
		std::vector<std::pair<unsigned int,double>> simPairs;
		computeSimilarities(noObjects,simPairs,*objit,objects[*objit],objectsByFeature); 
		initTopk(simPairs,topkByNewObjectId[*objit],inTailSizeByObject[*objit],*objit);
		// updateTopk(topkByNewObjectId[*objit],simPairs,inTailSizeByObject[*objit]);
		updateLinkedTopks(*objit,simPairs,topkByNewObjectId,inTailSizeByObject);
		addToIndex(*objit,objects[*objit],objectsByFeature);
	}
}//end of dynamicIndex

void Computer::initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, topkType& outputTopk, std::pair<unsigned int,double>& inTailSize, unsigned int targetIndex){
	std::list<std::pair<unsigned int,double>> topk;
	//+(k-1) because of first index = 0.
	if(simPairs.size()>k){
		nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.begin()+k; ++simpit){
			topk.push_back(*simpit);
			if(inTailSize.second-simpit->second>kFudgeFactor){
				inTailSize.second = simpit->second;
				inTailSize.first = 1;
			} else if(fabs(simpit->second-inTailSize.second)<kFudgeFactor){
				++inTailSize.first;
			}
		}
		while(simpit!=simPairs.end()){
			if(simpit->second>=inTailSize.second-kFudgeFactor){
				topk.push_back(*simpit);
				inTailSize.second = simpit->second;
			}
			++simpit;
		}
	} else {
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
			topk.push_back(*simpit);
			if(simpit->second<inTailSize.second){
				inTailSize.second = simpit->second;
				inTailSize.first = 1;
			} else if(simpit->second == inTailSize.second){
				++inTailSize.first;
			}
		}
		//add dummy elements to avoid checking if list is <k for every list update
		if(topk.size()<k){
			inTailSize.first = 0;
			inTailSize.second = minSim;
			while(topk.size()<k){
				topk.push_back(std::make_pair(targetIndex,minSim));
				++inTailSize.first;
			}
		}
	}
	//transform to priority queue
	topkType topkQ(topk.begin(),topk.end());
	//simply add to back of structure. Can do this because targetIndexes are handled in the correct order.
	outputTopk = topkQ;
}//end of initTopk

void Computer::idUnfinishedObjects(std::vector<std::pair<unsigned int,double>>& inTailSizeByObject, std::vector<unsigned int>& unfinishedObjects, std::vector<bool>& finishedByNewObjectId, double threshold){
	std::vector<std::pair<unsigned int,double>>::iterator pit;
	unsigned int object = 0;
	for(pit=inTailSizeByObject.begin();pit!=inTailSizeByObject.end();++pit){
		if(pit->second<threshold-kFudgeFactor){
			finishedByNewObjectId[object]=false;
			unfinishedObjects.push_back(object);
			pit->first = 0;
			pit->second = maxSim;
		}
		++object;
	}
}//end of idUnfinishedObjects

void Computer::restoreObjects(std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator featit;
	unsigned int featureIndex = 0;
	for(featit=objectsByFeature.begin(); featit!=objectsByFeature.end(); ++featit){
		while(!featit->empty()){
			objects[featit->back().first].push_back(std::make_pair(featureIndex,featit->back().second));
			featit->pop_back();
		}
		++featureIndex;
	}
	std::vector<std::vector<std::pair<unsigned int,double>>>::iterator objit;
	for(objit=objects.begin(); objit!=objects.end(); ++objit){
		std::sort(objit->begin(), objit->end(), compare_pairs_first_sib);
	}
}//end of restoreObjects

void Computer::writeTopks_elimDups(std::string controlFileName, std::vector<topkType>& topkByNewObjectId, std::vector<unsigned int>& oldByNewObjectId){
	// current method takes n log k i.s.o n time complexity;
	std::ofstream controlFile(controlFileName);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByNewObjectId.begin(); topkit!=topkByNewObjectId.end(); ++topkit){
		std::unordered_map<unsigned int,bool> printed;
		printed[objectIndex] = true; //avoid adding object itself as neighbor
		controlFile << oldByNewObjectId[objectIndex];
		while(topkit->size()>0){
			unsigned int nid = topkit->top().first;
			if(!printed[nid]){
				controlFile << "," << oldByNewObjectId[nid];
				printed[nid] = true;
			}
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks_elimDups

void Computer::writeTopks_elimDups_withSim(std::string controlFileName, std::vector<topkType>& topkByNewObjectId, std::vector<unsigned int>& oldByNewObjectId){
	// current method takes n log k i.s.o n time complexity;
	std::ofstream controlFile(controlFileName);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByNewObjectId.begin(); topkit!=topkByNewObjectId.end(); ++topkit){
		std::unordered_map<unsigned int,bool> printed;
		printed[objectIndex] = true; //avoid adding object itself as neighbor
		controlFile << oldByNewObjectId[objectIndex];
		while(topkit->size()>0){
			unsigned int nid = topkit->top().first;
			if(!printed[nid]){
				controlFile << "," << oldByNewObjectId[nid] << "," << topkit->top().second;
				printed[nid] = true;
			}
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks_elimDups_withSim

void Computer::readObjects(std::string inputFileName, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& newByOldFeatureId, std::vector<double>& normByOldObjectId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<size_t>& sizeByNewObjectId){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int oldObjectId = 0;
	while(readObject(inputFile,object)){
		std::vector<std::pair<unsigned int,double>>* op = &objects[newByOldObjectId[oldObjectId]];
		std::vector<std::string>::iterator featit;
		size_t size = 0;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				isKey = false;
			} else {
				val = atof(featit->c_str())/normByOldObjectId[oldObjectId];
				op->push_back(std::make_pair(newByOldFeatureId[key],val));
				++size;
				isKey = true;
			}
		}
		sizeByNewObjectId[newByOldObjectId[oldObjectId]] = size;
		std::sort(op->begin(),op->end(),compare_pairs_first_sib);
		++oldObjectId;
	}
	inputFile.close();
}//end of readObjects

void Computer::mapFeatures_bgf(std::vector<std::pair<unsigned int, double>>& featureValPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId,std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::vector<double>& maxWByNewFeatureId){
	std::sort(featureValPairs.begin(),featureValPairs.end(),compare_pairs_second_bib);
	std::vector<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newFeatureId = 0;
	for(pit=featureValPairs.begin(); pit!=featureValPairs.end(); ++pit){
		oldByNewFeatureId[newFeatureId] = pit->first;
		newByOldFeatureId[pit->first] = newFeatureId;
		maxWByNewFeatureId[newFeatureId] = maxWByOldFeatureId[pit->first];
		++newFeatureId;
	}
}//end of mapFeatures

void Computer::mapObjects_bgf(std::vector<std::pair<unsigned int, double>>& objectValPairs, std::vector<unsigned int>& newByOldObjectId, std::vector<unsigned int>& oldByNewObjectId,std::unordered_map<unsigned int,double>& maxWByOldObjectId, std::vector<double>& maxWByNewObjectId){
	std::sort(objectValPairs.begin(),objectValPairs.end(),compare_pairs_second_bib);
	std::vector<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newObjectId = 0;
	for(pit=objectValPairs.begin(); pit!=objectValPairs.end(); ++pit){
		oldByNewObjectId[newObjectId] = pit->first;
		newByOldObjectId[pit->first] = newObjectId;
		maxWByNewObjectId[newObjectId] = maxWByOldObjectId[pit->first];
		++newObjectId;
	}
}//end of mapFeatures

void Computer::extractGlobalStats(std::string inputFileName, unsigned int& noObjects, unsigned int& noFeatures, std::vector<double>& normByOldObjectId, std::vector<std::pair<unsigned,double>>& featureValPairs, std::vector<std::pair<unsigned int,double>>& objectValPairs, std::unordered_map<unsigned int,double>& maxWByOldFeatureId, std::unordered_map<unsigned int,double>& maxWByOldObjectId){

	std::unordered_map<unsigned int,unsigned int> countByOldFeatureId;

	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	noObjects = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		std::vector<std::pair<unsigned int,double>> temp;
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str());
				++countByOldFeatureId[key];
				isKey = false;
			} else {
				val = atof(featit->c_str());
				sqnorm += pow(val,2.0);
				temp.push_back(std::make_pair(key,val));
				isKey = true;
			}
		}
		double norm = sqrt(sqnorm);
		normByOldObjectId.push_back(norm);
		std::vector<std::pair<unsigned int,double>>::iterator pit;
		double normalized = 0;
		for(pit=temp.begin(); pit!=temp.end(); ++pit){
			normalized = pit->second/norm;
			maxWByOldObjectId[noObjects] = std::max(normalized,maxWByOldObjectId[noObjects]);
			maxWByOldFeatureId[pit->first] = std::max(normalized,maxWByOldFeatureId[pit->first]);
		}
		++noObjects;
	}
	inputFile.close();
	noFeatures = countByOldFeatureId.size();
	std::unordered_map<unsigned int,unsigned int>::iterator upit;
	for(upit=countByOldFeatureId.begin(); upit!=countByOldFeatureId.end(); ++upit) featureValPairs.push_back(std::make_pair(upit->first,(double)upit->second));
	std::unordered_map<unsigned int,double>::iterator vpit;
	for(vpit=maxWByOldObjectId.begin(); vpit!=maxWByOldObjectId.end(); ++vpit) objectValPairs.push_back(*vpit);
}//end of extractGlobalStats

void Computer::completeSimilarities(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::vector<std::pair<unsigned int,double>>>& objects, std::vector<std::pair<unsigned int,double>>& targetFeatures, unsigned int noFeatures){
	// unsigned int targetSize = targetFeatures.size();
	// double targetMaxVal = maxValByObjectId[targetIndex];
	// double targetMinsim = inTailSizeByObject[targetIndex].second;

	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		// std::cout << "will make dot of " << simpit->first << " and targetIndex" << std::endl << std::flush;
		// if(simpit->second+std::min(targetSize,objects[simpit->first].size())*maxValByObjectId[simpit->first]*targetMaxVal > std::min(inTailSizeByObject[simpit->first].second,targetMinSim)-kFudgeFactor){
			simpit->second  += dot(objects[simpit->first],targetFeatures);
		// }
	}
}//end of completeSimliarities

double Computer::half_dense_dot(std::vector<std::pair<unsigned int,double>>& v1, std::unordered_map<unsigned int,double>& v2){
	//assumes v1 and v2 are sorted from low to high key (unsigned int).
	double dot=0;
	std::vector<std::pair<unsigned int,double>>::iterator vit1;
	for(vit1=v1.begin(); vit1!=v1.end(); ++vit1) dot += vit1->second*v2[vit1->first];
	return dot;
}

void Computer::mapFeatures(std::vector<std::pair<unsigned int, double>>& featureValPairs, std::vector<unsigned int>& newByOldFeatureId, std::vector<unsigned int>& oldByNewFeatureId){
	std::sort(featureValPairs.begin(),featureValPairs.end(),compare_pairs_second_bib);
	std::vector<std::pair<unsigned int, double>>::iterator pit;
	unsigned int newFeatureId = 0;
	for(pit=featureValPairs.begin(); pit!=featureValPairs.end(); ++pit){
		oldByNewFeatureId[newFeatureId] = pit->first;
		newByOldFeatureId[pit->first] = newFeatureId;
		++newFeatureId;
	}
}//end of mapFeatures

void Computer::addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double minSim, std::vector<double>& maxValByNewFeatureId, std::vector<std::vector<std::pair<unsigned int,double>>>& objects){
	// std::cout << "in add to index" << std::endl << std::flush;
	double b = 0;
	unsigned int remainingSize = 0;
	std::vector<std::pair<unsigned int, double>>::iterator stayit;
	stayit = targetFeatures.begin();
	do{
		b += stayit->second*maxValByNewFeatureId[stayit->first];
		++stayit;
		++remainingSize;
	}while((stayit!=targetFeatures.end()) && (b<minSim));

	if(b>=minSim){
		remainingSize -= 1;
		unsigned int size = targetFeatures.size();
		while(size>remainingSize){
			objectsByFeature[targetFeatures.back().first].push_back(std::make_pair(targetIndex,targetFeatures.back().second));
			targetFeatures.pop_back();
			--size;
		}
	}
}//end of addToIndex

void Computer::processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures, double norm, std::vector<unsigned int>& newByOldFeatureId){
	//object = [key,val,key,val, ... , key,val]
	std::vector<std::string>::iterator featit;
	bool isKey = true;
	unsigned int key = 0;
	double val = 0; 
	for(featit=object.begin(); featit!=object.end(); ++featit){
		if(isKey){
			key = atoi(featit->c_str()); 
			isKey = false;
		}
		else{
			val = atof(featit->c_str())/norm; 
			targetFeatures.push_back(std::make_pair(newByOldFeatureId[key],val));
			isKey = true;
		}
	} //end of for
	std::sort(targetFeatures.begin(),targetFeatures.end(),compare_pairs_first_sib);
}//end of processObject

void Computer::dynamicIndex_heap(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm dynamicIndex" << std::endl << std::flush;
	std::cout << std::setprecision(15) << std::endl << std::flush;
	//initalize timer
	clock_t start = clock();
	//first pass over data to count number of objects and features and compute object norms
	std::tuple<unsigned int,unsigned int,std::vector<double>> globalInfo; //#objects, #features
	std::cout << "before getGlobalInfo" << std::endl << std::flush;
	globalInfo = getGlobalInfo(inputFileName);
	unsigned int noObjects = std::get<0>(globalInfo);
	unsigned int noFeatures = std::get<1>(globalInfo);
	std::vector<double> norms = std::get<2>(globalInfo);
	//initialize vectors of correct size (alternative:do not predefine size)

	std::cout << "number of features = " << noFeatures << std::endl << std::flush;

	std::vector<std::vector<std::pair<unsigned int,double>>> objectsByFeature (noFeatures);

	// std::cout << "initial size of objectsByFeature[252825] = " << objectsByFeature[252825].size() << std::endl << std::flush;

	//topk-by-object data structure required
	std::vector<topkType> topkByObject;
	std::vector<std::pair<unsigned int,double>> inTailSizeByObject(noObjects,std::make_pair(0,maxSim));

	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "preproc_time[s]");

	//for all objects
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	// unsigned int sumOfSimPairs = 0;
	unsigned int targetIndex = 0;
	std::cout << "before loop over inputFile" << std::endl << std::flush;
	while(readObject(inputFile,object)){

		// std::cout << "targetIndex = " << targetIndex << std::endl << std::flush;

		//process object
		std::vector<std::pair<unsigned int,double>> targetFeatures;
		// std::cout << "before processObject" << std::endl << std::flush;
		processObject(object,targetFeatures,norms[targetIndex]);

		// std::cout << "c1" << std::endl << std::flush;

		//compute similarities with previous objects
		std::vector<std::pair<unsigned int,double>> simPairs;
		// std::cout << "before computeSimilarities" << std::endl << std::flush;
		computeSimilarities_dynamicIndex(noObjects,simPairs,targetIndex,targetFeatures,objectsByFeature); 

		// std::cout << "c2" << std::endl << std::flush;

		// if(targetIndex==624425){
		// 	std::cout<<"simPairs 624 425: " << std::endl << std::flush;
		// 	std::vector<std::pair<unsigned int,double>>::iterator simpit;
		// 	for(simpit = simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		// 		std::cout<< simpit->first << "," << simpit->second << std::endl << std::flush;
		// 	}
		// }

		// sumOfSimPairs += simPairs.size();

		// std::cout << simPairs.size() << "," ;
		//initalize top-k of object
		// std::cout << "before initTopk" << std::endl << std::flush;
		initTopk(simPairs,topkByObject,inTailSizeByObject[targetIndex],targetIndex);

		// std::cout << "c3" << std::endl << std::flush;

		//update top-k of existing objects : loop over vector and check = 0 or keep separate set of adapted objects?
		// std::cout << "before updateLinkedTopks" << std::endl << std::flush;
		updateLinkedTopks(targetIndex,simPairs,topkByObject,inTailSizeByObject);

		// std::cout << "c4" << std::endl << std::flush;

		//add object to inverted indices
		// std::cout << "before addToIndex" << std::endl << std::flush;
		addToIndex(targetIndex,targetFeatures,objectsByFeature);

		// std::cout << "c5" << std::endl << std::flush;

		++targetIndex;
	}
	//output average number of simPairs
	// double avNoSimPairs = (double)sumOfSimPairs/(double)targetIndex;
	// double pctSimPairs = floor(avNoSimPairs*100/(double)targetIndex);
	// std::cout << "the average number of simPairs = " << avNoSimPairs << ". Taking into account there are " << targetIndex << "objects, this is " << pctSimPairs << "pct of the objects." << std::endl << std::flush;

	// std::cout << std::endl << std::flush;
	std::cout << "before write" << std::endl << std::flush;
	writeTopks_elimDups(controlFileName,topkByObject);
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
	std::cout << "after writing" << std::endl << std::flush;
}// end of dynamicIndex_heap

void Computer::computeSimilarities_dynamicIndex(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	//iterator in common
	std::vector<std::pair<unsigned int,double>>::reverse_iterator featit;

	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	
	for(featit=targetFeatures.rbegin(); featit!=targetFeatures.rend(); ++featit){
		// if(targetIndex == 205768) std::cout << "c10" << std::endl << std::flush;
		// if(targetIndex == 205768) std::cout << "feature = " << featit->first << std::endl << std::flush;
		// if(targetIndex == 205768) std::cout << "size of postings list = " <<  objectsByFeature[featit->first].size() << std::endl << std::flush;
		// if(targetIndex == 205768) std::cout << "c101" << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			// if(targetIndex == 205768) std::cout << "c11" << std::endl << std::flush;
			// if(targetIndex == 205768) std:: cout << "similarities has " << similarities.size() << " items" << std::endl << std::flush;
			// if(targetIndex == 205768) std::cout << "c111" << std::endl << std::flush;
			// if(targetIndex == 205768) std::cout << ", while candit->first = " << candit->first << std::endl << std::flush;
			if(similarities[candit->first]<kFudgeFactor){
				// if(targetIndex == 205768) std::cout << "c12" << std::endl << std::flush;
				linkedObjects.push_back(std::get<0>(*candit));
				// if(targetIndex == 205768) std::cout << "c13" << std::endl << std::flush;
			}
			// if(targetIndex == 205768) std::cout << "c14" << std::endl << std::flush;
			similarities[candit->first] += (featit->second)*(candit->second);	
			// if(targetIndex == 205768) std::cout << "c15" << std::endl << std::flush;
		}
	}

	//transform sparse representation for quick looping
	simPairs.resize(linkedObjects.size()); //no targetIndex because dynamic
	for(unsigned int i = 0; i<simPairs.size(); ++i){
		simPairs[i] = std::make_pair(linkedObjects[i],similarities[linkedObjects[i]]);
	}
}//end of computeSimilarities_dynamicIndex

void Computer::writeTopks(std::string controlFileName, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject){
	std::ofstream controlFile(controlFileName);
	std::vector<std::list<std::pair<unsigned int,double>>>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByObject.begin(); topkit!=topkByObject.end(); ++topkit){
		controlFile << objectIndex;
		std::list<std::pair<unsigned int,double>>::iterator nit;
		for(nit=(*topkit).begin(); nit!=(*topkit).end(); ++nit) controlFile << "," << nit->first;
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks

void Computer::writeTopks_elimDups(std::string controlFileName, std::vector<topkType>& topkByObject){
	// current method takes n log k i.s.o n time complexity;
	std::ofstream controlFile(controlFileName);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByObject.begin(); topkit!=topkByObject.end(); ++topkit){
		std::unordered_map<unsigned int,bool> printed;
		printed[objectIndex] = true; //avoid adding object itself as neighbor
		controlFile << objectIndex;
		while(topkit->size()>0){
			unsigned int nid = topkit->top().first;
			if(!printed[nid]){
				controlFile << "," << nid;
				printed[nid] = true;
			}
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks

void Computer::writeTopks_withSim(std::string controlFileName, std::vector<topkType>& topkByObject){
	// current method takes n log k i.s.o n time complexity;
	std::ofstream controlFile(controlFileName);
	controlFile.precision(15);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByObject.begin(); topkit!=topkByObject.end(); ++topkit){
		controlFile << objectIndex;
		while(topkit->size()>0){
			controlFile << "," << topkit->top().first << "," << topkit->top().second; //
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks

void Computer::writeTopks(std::string controlFileName, std::vector<topkType>& topkByObject){
	// current method takes n log k i.s.o n time complexity;
	std::unordered_map<unsigned int,bool> printed;
	std::ofstream controlFile(controlFileName);
	std::vector<topkType>::iterator topkit;
	unsigned int objectIndex = 0;
	for(topkit=topkByObject.begin(); topkit!=topkByObject.end(); ++topkit){
		controlFile << objectIndex;
		while(topkit->size()>0){
			controlFile << "," << topkit->top().first;
			topkit->pop();
		}
		controlFile << std::endl << std::flush;
		++objectIndex;
	}
	controlFile.close();
}//end of writeTopks


void Computer::addToIndex(unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	std::vector<std::pair<unsigned int, double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		objectsByFeature[featit->first].push_back(std::make_pair(targetIndex,featit->second));
	}
}//end of addToIndex

void Computer::resetTopk(std::list<std::pair<unsigned int,double>>& topk, double& min, unsigned int& nMin){
	if(topk.size()-nMin==k){
		double old_min = min;
		min = maxSim;
		std::list<std::pair<unsigned int,double>>::iterator topkit=topk.begin();
		while(topkit!=topk.end()){
			if(topkit->second == old_min){
				topkit = topk.erase(topkit);
			} else if(topkit->second<min){
				min = topkit->second;
				nMin = 1;
				++topkit;
			} else if (topkit->second == min){
				++nMin;
				++topkit;
			} else {
				++topkit;
			}
		} // end of for over topk
	} 
}//end of resetTopk

void Computer::updateTopk(topkType& topk, unsigned int candidate, double similarity , std::pair<unsigned int,double>& inTailSize, unsigned int topkId){
	if(similarity>=inTailSize.second-kFudgeFactor){
		// if(topkId==339182)std::cout << "cand "<< candidate << " will be added to topk because similarity " << similarity << " >= " << inTailSize.second-kFudgeFactor << std::endl << std::flush;
		// if(topkId==339182)std::cout << "valid for adding to topk" << std::endl << std::flush;
		if(similarity>inTailSize.second+kFudgeFactor){
			if(inTailSize.first == 1){
					// if(topkId==339182)std::cout << "1: inTailSize == 1 and " << similarity << " > " << inTailSize.second-kFudgeFactor << std::endl << std::flush;
				do{
					topk.pop();
				} while(topk.size()>=k);
				topk.push(std::make_pair(candidate,similarity));
				inTailSize.second = topk.top().second;
				resetTopk(topk,inTailSize);
			} else {
				// if(topkId==339182)std::cout << "2: inTailSize != 1 and " << similarity << " > " << inTailSize.second-kFudgeFactor << std::endl << std::flush;

				--inTailSize.first;
				topk.push(std::make_pair(candidate,similarity));
			}

		} else if(similarity>inTailSize.second){
			if(inTailSize.first == 1){
				// if(topkId==339182)if(topkId==339182)std::cout << "3: inTailSize == 1 and " << similarity << " > " << inTailSize.second << std::endl << std::flush;

				topk.push(std::make_pair(candidate,similarity));
				inTailSize.second = similarity;
				while(topk.top().second<inTailSize.second-kFudgeFactor){
					topk.pop();
				}
			} else {
				// if(topkId==339182)std::cout << "4: inTailSize != 1 and " << similarity << " > " << inTailSize.second << std::endl << std::flush;

				topk.push(std::make_pair(candidate,similarity));
				handleNewMin(topk,inTailSize);
			}
		} else {
			// if(topkId==339182)std::cout << "5: " << similarity << " == " << inTailSize.second << std::endl << std::flush;

			topk.push(std::make_pair(candidate,similarity));
		}
	}
	// else if(topkId==339182) std::cout << "NOT valid for adding to topk because similarity = " << similarity << " and limit = " << inTailSize.second-kFudgeFactor << std::endl << std::flush;
}//end of updateTopk

void Computer::handleNewMin(topkType& topk, std::pair<unsigned int,double>& inTailSize){
	std::vector<std::pair<unsigned int,double>> bench;
	while(topk.size()>k){
		bench.push_back(topk.top());
		topk.pop();
	}
	inTailSize.second = topk.top().second;
	inTailSize.first = 0;
	bool same = true;
	while((topk.size()>0) && same){
		if(topk.top().second<=inTailSize.second+kFudgeFactor){
			bench.push_back(topk.top());
			topk.pop();
			++inTailSize.first;
		}
		else{
			same = false;
		}
	}
	std::vector<std::pair<unsigned int,double>>::reverse_iterator benchit = bench.rbegin();
	while((benchit->second>=inTailSize.second-kFudgeFactor) && (benchit!=bench.rend())){
		topk.push(*benchit);
		++benchit;
	}
}//end of handleNewMin

void Computer::resetTopk(topkType& topk, std::pair<unsigned int,double>& inTailSize){
	std::vector<std::pair<unsigned int,double>> bench;
	bool same = true;
	inTailSize.first = 0;
	while((topk.size()>0) && same){
		if(topk.top().second<=inTailSize.second+kFudgeFactor){
			bench.push_back(topk.top());
			topk.pop();
			++inTailSize.first;
		}
		else{
			same = false;
		}
	}
	std::vector<std::pair<unsigned int,double>>::iterator benchit;
	for(benchit=bench.begin(); benchit!=bench.end(); ++benchit){
		topk.push(*benchit);
	}
}

void Computer::updateTopk(std::list<std::pair<unsigned int,double>>& topk, unsigned int candidate, double similarity , double& min, unsigned int& nMin){
	if(topk.size()<k){
		topk.push_back(std::make_pair(candidate,similarity));
		if(similarity<min) min=similarity,nMin=1;
		else if(similarity==min) ++nMin;
	} else {
		if(similarity>min){
			topk.push_back(std::make_pair(candidate,similarity));
			resetTopk(topk,min,nMin);
		}else if(similarity==min){
			topk.push_back(std::make_pair(candidate,similarity));
			++nMin;
		}
	}	
}//end of updateTopk

void Computer::updateLinkedTopks(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject, std::vector<std::pair<double,unsigned int>>& minSimByObject){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
		updateTopk(topkByObject[simpit->first],targetIndex,simpit->second,minSimByObject[simpit->first].first,minSimByObject[simpit->first].second);
	}
}//end of updateLInkedTopks

void Computer::updateLinkedTopks(unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<topkType>& topkByObject, std::vector<std::pair<unsigned int,double>>& inTailSizeByObject){
	std::vector<std::pair<unsigned int,double>>::iterator simpit;
	for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
			// std::cout << "will update topk of "<< simpit->first << " with "<< targetIndex << " Similarity = " << simpit->second << std::endl << std::flush;
		updateTopk(topkByObject[simpit->first],targetIndex,simpit->second,inTailSizeByObject[simpit->first],simpit->first);
	}
}//end of updateLInkedTopks

void Computer::initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<topkType>& topkByObject, std::pair<unsigned int,double>& inTailSize, unsigned int targetIndex){
	std::list<std::pair<unsigned int,double>> topk;
	// if (targetIndex==339182) std::cout << "in initTopk of 339182" << std:: endl << std::flush;
	//+(k-1) because of first index = 0.
	if(simPairs.size()>k){
		nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.begin()+k; ++simpit){
			// if(targetIndex==339182) std::cout << " 1:neighbor = " << simpit->first << " with similarity " << simpit->second << std::endl << std::flush;
			// if( (targetIndex==339182) && (simpit->first==238951) ) std::cout << "502 968 in initial top-k of 629094" << std::endl << std::flush;
			topk.push_back(*simpit);
			if(inTailSize.second-simpit->second>kFudgeFactor){
				// if(targetIndex==339182) std::cout << "similarity " << simpit->second << " deemed SMALLER than current minsim, which is " << inTailSize.second << std::endl << std::flush;
				inTailSize.second = simpit->second;
				inTailSize.first = 1;
			} else if(fabs(simpit->second-inTailSize.second)<kFudgeFactor){
				// if(targetIndex==339182) std::cout << "similarity " << simpit->second << " deemed EQUAL to current minsim, which is " << inTailSize.second << std::endl << std::flush;
				++inTailSize.first;
			}
		}
		while(simpit!=simPairs.end()){
			std::cout.precision(15);
			// if(targetIndex==339182) std::cout << " 2:neighbor = " << simpit->first << " with similarity " << simpit->second << std::endl << std::flush;
			// if( (targetIndex==339182) && (simpit->first==238951) ) std::cout << "502 968 considerd after top-k of 339182. similarity is " << simpit->second << ", minsim = " << inTailSize.second << std::endl << std::flush;
			if(simpit->second>=inTailSize.second-kFudgeFactor){
				// if(targetIndex==339182) std::cout << " 2:neighbor = " << simpit->first << " with similarity " << simpit->second << std::endl << std::flush;
				// if( (targetIndex==339182) && (simpit->first==238951) ) std::cout << "502 968 added to top-k of 339182 after compared equal to k-th value" << std::endl << std::flush;
				topk.push_back(*simpit);
				inTailSize.second = simpit->second;
			}
			++simpit;
		}
	} else {
		// if( (targetIndex==339182) ) std::cout << "initial top-k of 339182 smaller than k" << std::endl << std::flush;
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
			topk.push_back(*simpit);
			if(simpit->second<inTailSize.second){
				inTailSize.second = simpit->second;
				inTailSize.first = 1;
			} else if(simpit->second == inTailSize.second){
				++inTailSize.first;
			}
		}
		//add dummy elements to avoid checking if list is <k for every list update
		if(topk.size()<k){
			inTailSize.first = 0;
			inTailSize.second = minSim;
			while(topk.size()<k){
				topk.push_back(std::make_pair(targetIndex,minSim));
				++inTailSize.first;
			}
		}
	}
	//
	// if( (targetIndex==339182) ){
	// 	std::list<std::pair<unsigned int,double>>::iterator topkit;
	// 	for(topkit=topk.begin();topkit!=topk.end();++topkit){
	// 		// std::cout << "top-k of 339182 contains " << topkit->first << " with similarity " << topkit->second << std::endl << std::flush;
	// 	}
	// 	// std::cout << "inTailSize = " << inTailSize.first << ", minSim = " << inTailSize.second << std::endl << std::flush;
	// }
	//transform to priority queue
	topkType topkQ(topk.begin(),topk.end());
	//simply add to back of structure. Can do this because targetIndexes are handled in the correct order.
	topkByObject.push_back(topkQ);
}//end of initTopk

void Computer::initTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<std::list<std::pair<unsigned int,double>>>& topkByObject, std::pair<double,unsigned int>& minSim){
	std::list<std::pair<unsigned int,double>> topk;
	//+(k-1) because of first index = 0.
	if(simPairs.size()>k){
		nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.begin()+k; ++simpit){
			topk.push_back(*simpit);
			if(simpit->second<minSim.first){
				minSim.first = simpit->second;
				minSim.second = 1;
			} else if(simpit->second == minSim.first){
				++minSim.second;
			}
		}
		while(simpit!=simPairs.end()){
			if(simpit->second>=minSim.first-kFudgeFactor){
				topk.push_back(*simpit);
				minSim.first = simpit->second;
				++(minSim.second);
			}
			++simpit;
		}
	} else {
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
			topk.push_back(*simpit);
			if(simpit->second<minSim.first){
				minSim.first = simpit->second;
				minSim.second = 1;
			} else if(simpit->second == minSim.first){
				++minSim.second;
			}
		}
	}
	//simply add to back of structure. Can do this because targetIndexes are handled in the correct order.
	topkByObject.push_back(topk);
}//end of initTopk

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	// std::cout << "in compute similarities" << std::endl << std::flush;
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			// std::cout << "candidate: " << candit->first << "value: " << candit->second << std::endl << std::flush; 
			similarities[candit->first] += (featit->second)*(candit->second);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::computeSimilarities(unsigned int noObjects, std::vector<std::pair<unsigned int,double>>& simPairs, std::vector<double>& norms, unsigned int targetIndex, std::vector<std::pair<unsigned int, double>>& targetFeatures, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	//dense representation for quick update
	std::vector<double> similarities(noObjects,0);
	std::vector<unsigned int> linkedObjects;
	std::vector<std::pair<unsigned int,double>>::iterator featit;
	for(featit=targetFeatures.begin(); featit!=targetFeatures.end(); ++featit){
		std::vector<std::pair<unsigned int,double>>::iterator candit;
		for(candit=objectsByFeature[featit->first].begin(); candit!=objectsByFeature[featit->first].end(); ++candit){
			if(similarities[candit->first]==0) linkedObjects.push_back(candit->first);
			similarities[candit->first] += (featit->second)*(candit->second)/(norms[candit->first]*norms[targetIndex]);
		}
	}
	//transform sparse representation for quick looping
	std::vector<std::pair<unsigned int,double>> topk;
	std::vector<unsigned int>::iterator linkit;
	for(linkit=linkedObjects.begin(); linkit!=linkedObjects.end(); ++linkit){
		if(*linkit!=targetIndex) simPairs.push_back(std::make_pair(*linkit,similarities[*linkit]));
	}
}//end of computeSimilarities

void Computer::processObject(std::vector<std::string>& object, std::vector<std::pair<unsigned int, double>>& targetFeatures){
	//object = [key,val,key,val, ... , key,val]
	std::vector<std::string>::iterator featit;
	bool isKey = true;
	unsigned int key = 0;
	double val = 0; 
	for(featit=object.begin(); featit!=object.end(); ++featit){
		if(isKey){
			key = atoi(featit->c_str()); 
			isKey = false;
		}
		else{
			val = atof(featit->c_str()); 
			targetFeatures.push_back(std::make_pair(key,val));
			isKey = true;
		}
	} //end of for
}//end of processObject

void Computer::baseline_p(std::string inputFileName, std::string timingFileName, std::string controlFileName){
	std::cout << "started algorithm baseline_p" << std::endl << std::flush;
	//initalize timer
	clock_t start = clock();
	//first pass over data to count number of objects and features and compute object norms
	std::tuple<unsigned int,unsigned int,std::vector<double>> globalInfo; //#objects, #features
	std::cout << "before getGlobalInfo" << std::endl << std::flush;
	globalInfo = getGlobalInfo(inputFileName);
	unsigned int noObjects = std::get<0>(globalInfo);
	unsigned int noFeatures = std::get<1>(globalInfo);
	std::vector<double> norms = std::get<2>(globalInfo);

	//initialize vectors of correct size (alternative:do not predefine size)
	std::vector<std::vector<std::pair<unsigned int,double>>> objectsByFeature (noFeatures);
	std::vector<std::vector<std::pair<unsigned int,double>>> featuresByObject (noObjects);
	//fill vectors during second pass over data
	std::cout << "before fillDoubleIndex" << std::endl << std::flush;
	fillDoubleIndex(inputFileName, featuresByObject, objectsByFeature,norms);
	//open file to print controls to
	std::ofstream controlFile(controlFileName);
	//initialize statistics
	// double avSimPairsSize = 0;
	// unsigned int segmentProcessingCounter = 0;
	//start scoring
	unsigned int targetIndex;
	std::cout << "before loop over targetIndex" << std::endl << std::flush;
	for(targetIndex=0; targetIndex<noObjects; ++targetIndex){
		std::vector<double> similarities(noObjects,0);
		std::list<unsigned int> topk;

		std::vector<std::pair<unsigned int,double>> simPairs;
		computeSimilarities(noObjects,simPairs,targetIndex,featuresByObject[targetIndex],objectsByFeature); 

		// avSimPairsSize = (double)(avSimPairsSize*segmentProcessingCounter + simPairs.size())/(double)(segmentProcessingCounter+1);
		// ++segmentProcessingCounter;

		//extractTopk
		extractTopk(simPairs,topk);

		//write results to control file
		// std::cout << "before writeTopk" << std::endl << std::flush;
		writeTopk(targetIndex,topk,controlFile);

	}//end for over all targets
	// std::cout << "average number of accumulators per object = " << avSimPairsSize << std::endl << std::flush;
	controlFile.close();
	writeTiming(timingFileName, (clock()-start)/double(CLOCKS_PER_SEC), "cpp_time[s]");
}//end of baseline_postSort

void Computer::fillDoubleIndex(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, std::vector<double>& norms){
	std::cout << "start fill double index" << std::endl << std::flush;
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0; 
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str()); 
				isKey = false;
			}
			else{
				val = atof(featit->c_str())/norms[objectIndex]; 
				objectsByFeature[key].push_back(std::make_pair(objectIndex,val));
				featuresByObject[objectIndex].push_back(std::make_pair(key,val));
				isKey = true;
			}
		} //end of for
		++ objectIndex;
	}//end of while
	inputFile.close();
}//end of buildInvertedIndices

void Computer::extractTopk(std::vector<std::pair<unsigned int,double>>& simPairs, std::list<unsigned int>& topk){
	//+(k-1) because of first index = 0.
	if(simPairs.size()>k){
		nth_element(simPairs.begin(),simPairs.begin()+(k-1),simPairs.end(),compare_pairs_second_bib);
		double minSim = simPairs[k-1].second;
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.begin()+k; ++simpit){
			topk.push_back(simpit->first);
		}
		while(simpit!=simPairs.end()){
			if(simpit->second>=minSim-kFudgeFactor) topk.push_back(simpit->first);
			++simpit;
		}
	} else {
		std::vector<std::pair<unsigned int,double>>::iterator simpit;
		for(simpit=simPairs.begin(); simpit!=simPairs.end(); ++simpit){
			topk.push_back(simpit->first);
		}
	}
}//end of extractTopk

void Computer::fillTopk(std::vector<double> norms, unsigned int targetIndex, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, std::vector<double>& similarities, std::list<unsigned int>& topk, std::vector<bool>& inTopk, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	while((featit!=featuresByObject[targetIndex].end()) && (topk.size()<k)){
		candit = objectsByFeature[featit->first].begin();
		while((candit!=objectsByFeature[featit->first].end()) && (topk.size()<k)){
			similarities[candit->first] += (featit->second)*(candit->second)/norms[candit->first];
			if((!inTopk[candit->first]) && (candit->first!=targetIndex)){
				topk.push_back(candit->first);
				inTopk[candit->first] = true;
			}
			++candit;
		}
		++featit;
	}
	//set feature back by one to allow finishing list of candidates for current feature
	--featit;
}//end of fillTopk

void Computer::initMinTopk(double& min, unsigned int& nMin, std::list<unsigned int>& topk, std::vector<double>& similarities){
	std::list<unsigned int>::iterator topkit;
	for(topkit=topk.begin();topkit!=topk.end();++topkit){
		if(similarities[*topkit]<min){
			min = similarities[*topkit];
			nMin = 1;
		} else if(similarities[*topkit]==min){
			nMin += 1;
		}
	}//end for
}//end of initMinTopk

void Computer::resetMinTopk(double& min, unsigned int& nMin, std::list<unsigned int>& topk, std::vector<double>& similarities, double startMin){
	if(topk.size()-nMin+1>=k){
		double old_min = min;
		min = startMin;
		std::list<unsigned int>::iterator topkit = topk.begin();
		while(topkit!=topk.end()){
			if(similarities[*topkit] == old_min){
				topkit = topk.erase(topkit);
			} else if(similarities[*topkit] < min){
				min = similarities[*topkit];
				nMin = 1;
				++topkit;
			} else if(similarities[*topkit] == min){
				nMin += 1;
				++topkit;
			} else {
				++topkit;
			}
		}//end while
	} 
}//end of initMinTopk

void Computer::updateTopk(std::vector<double> norms, unsigned int targetIndex, std::list<unsigned int>& topk, double& min, unsigned int& nMin, std::vector<double>& similarities, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature, double startMin){
	while(candit!=objectsByFeature[featit->first].end()){
		processOverlap(norms,targetIndex,topk,min,nMin,similarities,featit,candit,startMin);
		++candit;
	}
	++featit;
	// std::cout << "noFeatures:" << featuresByObject[targetIndex].size() << std::endl << std::flush;
	while(featit!=featuresByObject[targetIndex].end()){
		// std::cout << "noCandidates:" << objectsByFeature[featit->first].size() << std::endl << std::flush;
		candit = objectsByFeature[featit->first].begin();
		while(candit!=objectsByFeature[featit->first].end()){
			processOverlap(norms,targetIndex,topk,min,nMin,similarities,featit,candit,startMin);
			++candit;
		}
		++featit;
	}
}// end of updateTopk

void Computer::processOverlap(std::vector<double> norms, unsigned int targetIndex, std::list<unsigned int>& topk, double& min, unsigned int& nMin, std::vector<double>& similarities, std::vector<std::pair<unsigned int,double>>::iterator& featit, std::vector<std::pair<unsigned int,double>>::iterator& candit, double startMin){
	double old_sim  = similarities[candit->first];
	similarities[candit->first] += (featit->second)*(candit->second)/norms[candit->first];
	double new_sim = similarities[candit->first];
	if(new_sim>min){
		if(candit->first!=targetIndex){
			if(old_sim<min){
				topk.push_back(candit->first);
				resetMinTopk(min,nMin,topk,similarities,startMin);
			} else if (old_sim==min){
				resetMinTopk(min,nMin,topk,similarities,startMin);
			}
		}
	} else if((new_sim==min) && (candit->first!=targetIndex)){
		topk.push_back(candit->first);
	}
}//end of processOverlap

void Computer::writeTopk(unsigned int targetIndex, std::list<unsigned int>& topk, std::ofstream& controlFile){
	controlFile << targetIndex;
	std::list<unsigned int>::iterator topkit;
	for(topkit=topk.begin(); topkit!=topk.end(); ++topkit) controlFile << "," << *topkit;
	controlFile << std::endl << std::flush;
} // end of writeTopk

void Computer::writeTiming(std::string timingFileName, clock_t recordedTime, std::string timeDescription){
	std::ofstream timingFile(timingFileName, std::ofstream::out | std::ofstream::app);
	timingFile << timeDescription << "," << recordedTime << std::endl << std::flush;
	timingFile.close();
}//end of writeTiming

void Computer::writeDouble(std::string fileName, double doubleNumber, std::string doubleDescription){
	std::ofstream outputFile(fileName, std::ofstream::out | std::ofstream::app);
	outputFile << doubleDescription << "," << doubleNumber << std::endl << std::flush;
	outputFile.close();
}//end of writeTiming

std::tuple<unsigned int,unsigned int,std::vector<double>> Computer::getGlobalInfo(std::string inputFileName){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	std::set<unsigned int> features;
	std::vector<double> norms;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				features.insert(atoi(featit->c_str()));
				isKey = false;
			} else {
				sqnorm += pow(atof(featit->c_str()),2.0);
				isKey = true;
			}
		} //end of for
		norms.push_back(sqrt(sqnorm));
		++objectIndex;
	}//end of while
	inputFile.close();
	return std::make_tuple(objectIndex,features.size(),norms);
}//end of count

std::tuple<unsigned int,unsigned int,std::vector<double>> Computer::getGlobalInfo(std::string inputFileName, std::vector<double>& maxValByFeatureId, std::vector<std::pair<unsigned int, double>>& featureValPairs){
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	std::set<unsigned int> features;
	std::vector<double> norms;
	std::unordered_map<unsigned int,double> maxValByFeatureIdMap;
	std::unordered_map<unsigned int,double> valByFeatureMap;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		double sqnorm = 0;
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		std::vector<std::pair<unsigned,double>> temp;
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				features.insert(atoi(featit->c_str()));
				key = atoi(featit->c_str());
				++valByFeatureMap[key];
				isKey = false;
			} else {
				sqnorm += pow(atof(featit->c_str()),2.0);
				temp.push_back(std::make_pair(key,atof(featit->c_str())));
				isKey = true;
			}
		} //end of for
		double norm = sqrt(sqnorm);
		norms.push_back(norm);
		std::vector<std::pair<unsigned,double>>::iterator pit;
		double normalized = 0;
		for(pit=temp.begin(); pit!=temp.end(); ++pit){
			normalized = pit->second/norm;
			maxValByFeatureIdMap[pit->first] = std::max(maxValByFeatureIdMap[pit->first],normalized);
		}
		++objectIndex;
	}//end of while
	inputFile.close();
	std::unordered_map<unsigned int,double>::iterator mit;
	maxValByFeatureId.resize(features.size(),0);
	for(mit=maxValByFeatureIdMap.begin(); mit!=maxValByFeatureIdMap.end(); ++mit) maxValByFeatureId[mit->first] = mit->second;
	for(mit=valByFeatureMap.begin(); mit!=valByFeatureMap.end(); ++mit) featureValPairs.push_back(*mit);
	return std::make_tuple(objectIndex,features.size(),norms);
}//end of count

void Computer::fillDoubleIndex(std::string inputFileName, std::vector<std::vector<std::pair<unsigned int,double>>>& featuresByObject, std::vector<std::vector<std::pair<unsigned int,double>>>& objectsByFeature){
	std::cout << "start fill double index" << std::endl << std::flush;
	std::ifstream inputFile(inputFileName, std::ifstream::in);
	std::vector<std::string> object;
	unsigned int objectIndex = 0;
	while(readObject(inputFile,object)){
		//object = [key,val,key,val, ... , key,val]
		std::vector<std::string>::iterator featit;
		bool isKey = true;
		unsigned int key = 0;
		double val = 0; 
		for(featit=object.begin(); featit!=object.end(); ++featit){
			if(isKey){
				key = atoi(featit->c_str()); 
				isKey = false;
			}
			else{
				val = atof(featit->c_str()); 
				objectsByFeature[key].push_back(std::make_pair(objectIndex,val));
				featuresByObject[objectIndex].push_back(std::make_pair(key,val));
				isKey = true;
			}
		} //end of for
		++ objectIndex;
	}//end of while
	inputFile.close();
}//end of buildInvertedIndices

